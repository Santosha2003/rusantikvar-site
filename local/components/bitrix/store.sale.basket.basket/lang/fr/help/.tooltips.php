<?
$MESS ['PATH_TO_ORDER_TIP'] = "Le nom de chemin d'accиs de la page afin d'achиvement. Vous pouvez spйcifier le nom du fichier si la page est dans le rйpertoire courant.";
$MESS ['HIDE_COUPON_TIP'] = "Sйlectionnez \"Oui\" pour cacher le code coupon champ de saisie dans la page panier.";
$MESS ['COLUMNS_LIST_TIP'] = "Champs sйlectionnйs seront utilisйs comme titres de colonnes dans un panier contenu table.";
$MESS ['SET_TITLE_TIP'] = "Cochez cette option sera fixй le titre de la page \"Mon Panier\".";
$MESS ['QUANTITY_FLOAT_TIP'] = "Cochez cette option pour permettre le point de fractionnement de quantitйs.";
$MESS ['PRICE_VAT_INCLUDE_TIP'] = "Le contrфle de cette option spйcifie pour inclure la taxe dans les prix d'йtalage.";
$MESS ['PRICE_VAT_SHOW_VALUE_TIP'] = "Spйcifie pour montrer la valeur fiscale.";
?>