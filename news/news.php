<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Существуют монеты, которые представляют собой сплошную загадку. После свертывания НЭПа советское правительство задумало перейти к чеканке медно-никелевой монеты взамен серебряной. Серебро — валютный металл — требовалось для нужд индустриализации. Чтобы граждане не припрятывали серебряную мелочь, реформа готовилась втайне (на уцелевших документах стоит гриф «совершенно секретно»). В 1931 году в СССР в обращении появились медно-никелевые 10, 15 и 20 копеек. Монеты в 1 рубль и 50 копеек было решено вообще не чеканить. В архиве Ленинградского монетного двора сохранился штемпельный инструмент пробных 10 и 50 копеек, но ни единого образца самих монет. Однако несколько лет назад в одной частной коллекции обнаружился полтинник — единственный известный на сегодня экземпляр пробной чеканки 1929 года. В мае 2011 года на аукционе «Знакъ» он был продан покупателю, сделавшему ставку по телефону, за 10 млн рублей. Это рекордная цена на российскую или советскую монету за последние три года.");
$APPLICATION->SetTitle("Title");
?>
Text here....

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>