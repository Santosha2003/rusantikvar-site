<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<a class="social-link" href="https://vk.com/rrusantikvar" target="_blank" title="Мы во Вконтакте" aria-label="Мы во Вконтакте">
  <img src="/local/templates/antikvar/img/icon-vk.png" width="32" height="32" alt="Мы во Вконтакте" aria-hidden="true">
</a>

<a class="social-link" href="https://www.instagram.com/rusantikvar/" target="_blank" title="Мы в Instagram" aria-label="Мы в Instagram">
  <img src="/local/templates/antikvar/img/icon-instagram-filled.png" width="32" height="32" alt="Мы в Instagram" aria-hidden="true">
</a>

<a class="social-link" href="https://www.facebook.com/rusantikvar.rusantikvar" target="_blank" title="Мы в Facebook" aria-label="Мы в Facebook">
    <img src="/local/templates/antikvar/img/icon-facebook.png" width="32" height="32" alt="Мы в Facebook" aria-hidden="true">
</a>
