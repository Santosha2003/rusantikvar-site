<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Высокий рельеф");
?><p style="text-align: center;"><b><span style="font-size: 15pt;">Высокий рельеф.</span></b></p>
<div style="background-color: white">
<p style="text-align: center;"><a href="/upload/medialibrary/28f/28f5df438e5379cf31cbde527f45d9f9.jpg" rel="18-06-2020-3"><img alt="ДР-лот-389-МиМ-ав.jpg" src="/upload/medialibrary/28f/28f5df438e5379cf31cbde527f45d9f9.jpg" title="ДР-лот-389-МиМ-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/396/396f323cd529260cbbe2843529663f36.jpg" rel="18-06-2020-3"><img alt="ДР-лот-389-МиМ-ухо.jpg" src="/upload/medialibrary/396/396f323cd529260cbbe2843529663f36.jpg" title="ДР-лот-389-МиМ-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/4e1/4e1812f7ddc42d4d16e60c220b0cbad0.jpg" rel="18-06-2020-3"><img alt="ДР-лот-389-МиМ-рв.jpg" src="/upload/medialibrary/4e1/4e1812f7ddc42d4d16e60c220b0cbad0.jpg" title="ДР-лот-389-МиМ-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;"></p>
<p style="text-align: center;">Вес-<b>12,3 гр</b>. диаметр-<b>28,3 мм.</b><br>	 &nbsp;&nbsp; Вензель на погоне Императора Николая II не читается , крест 
на шапке 
Михаила&nbsp; заходит на сплошную черту. Пунктирная линия на аверсе начинается с
 штриха им же и заканчивается. На реверсе уменьшенный шрифт.</p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/bd9/bd9fca138b9fa744347585c18377af11.jpg" rel="16-08-2021-1"><img alt="300-лет-ДР-МШ-ав.jpg" src="/upload/medialibrary/bd9/bd9fca138b9fa744347585c18377af11.jpg" title="300-лет-ДР-МШ-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/9f8/9f84e1e6aa2aa1ba780d6b7096bbd8a6.jpg" rel="16-08-2021-1"><img alt="300-лет-ДР-МШ-ухо.jpg" src="/upload/medialibrary/9f8/9f84e1e6aa2aa1ba780d6b7096bbd8a6.jpg" title="300-лет-ДР-МШ-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/984/984791b59476eecee1abadc8d5194192.jpg" rel="16-08-2021-1"><img alt="300-лет-ДР-МШ-рв.jpg" src="/upload/medialibrary/984/984791b59476eecee1abadc8d5194192.jpg" title="300-лет-ДР-МШ-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>12,1 гр.</b> диаметр-<b>28,32 мм.</b><br>Вензель "А" на погоне Императора Николая II не четко выражен, крест на 
шапке Михаила не касается сплошной черты. На обрезе медали инициалы 
мастерской "МШ".<br>
Пунктирная линия на аверсе начинается с пунктира им же и заканчивается. <br></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/4ce/4ce00fc0e0b3f7526ff53abb6dd46139.jpg" rel="10.09.2021-1"><img alt="4-ав.jpg" src="/upload/medialibrary/4ce/4ce00fc0e0b3f7526ff53abb6dd46139.jpg" title="4-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/c7d/c7d3a26a28b3420f71be2efb74163140.jpg" rel="10.09.2021-1"><img alt="4-ухо.jpg" src="/upload/medialibrary/c7d/c7d3a26a28b3420f71be2efb74163140.jpg" title="4-ухо.jpg" width="174" height="175" align="middle"></a><a href="/upload/medialibrary/e55/e558406966c0cf5dac8a757a31e43d5a.jpg" rel="10.09.2021-1"><img alt="4-рв.jpg" src="/upload/medialibrary/e55/e558406966c0cf5dac8a757a31e43d5a.jpg" title="4-рв.jpg" width="185" height="185" align="middle"></a></p><p style="text-align: center;"></p>
<p style="text-align: center;">Вес-<b>10,9 гр.</b> диаметр-<b>28 мм.</b><br>&nbsp;&nbsp; Аверс: Вензель на погоне Императора Николая II не четкий, Крест на
шапке Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается
с пунктира им же и заканчивается.<br>&nbsp;&nbsp; Реверс: цифра 3 имеет небольшой наклон влево, буква Ъ наклонена
вправо. Сама надпись со сдвигом.</p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/56d/56d94ab514d77f41c157c567be92fb75.jpg" rel="10.09.2021-2"><img alt="3МШ-ав.jpg" src="/upload/medialibrary/56d/56d94ab514d77f41c157c567be92fb75.jpg" title="3МШ-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/b5b/b5b0dbc5d89be4b31a2d35af7ce4aeff.jpg" rel="10.09.2021-2"><img alt="3МШ-ухо.jpg" src="/upload/medialibrary/b5b/b5b0dbc5d89be4b31a2d35af7ce4aeff.jpg" title="3МШ-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/20c/20c3256db4c57af29060b7759c6c0483.jpg" rel="10.09.2021-2"><img alt="3МШ-рв.jpg" src="/upload/medialibrary/20c/20c3256db4c57af29060b7759c6c0483.jpg" title="3МШ-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>11,9 гр.</b> диаметр-<b>28,22 мм.</b></p>
<p style="text-align: center;">&nbsp;Вензель «А» на погоне Императора Николая II, Крест на шапке
Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается с
точки, ею же и заканчивается. На обрезе инициалы «МШ»</p>
<p></p>

<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/50b/50ba3b0acae32357fa21b06d4961951f.jpg" rel="10.09.2021-3"><img alt="5-ав.jpg" src="/upload/medialibrary/50b/50ba3b0acae32357fa21b06d4961951f.jpg" title="5-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/5a1/5a1b1dc44a537e98cd464023ded45374.jpg" rel="10.09.2021-3"><img alt="5-ухо.jpg" src="/upload/medialibrary/5a1/5a1b1dc44a537e98cd464023ded45374.jpg" title="5-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/238/23850fa842a87ecb9226c7a9b2fc23c4.jpg" rel="10.09.2021-3"><img alt="5-рв.jpg" src="/upload/medialibrary/238/23850fa842a87ecb9226c7a9b2fc23c4.jpg" title="5-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>10,7 гр.</b> диаметр-<b>27,71 мм.<br></b></p>
<p style="text-align: center;">

</p>
<p>Вензель на погоне Императора Николая II не четкий, Крест на шапке
Михаила касается сплошной черты. Пунктирная линия на аверсе начинается с точки ею
же и заканчивается.</p>

<p> </p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/7df/7dffa940c6ffa5e5547d1624459e2b02.jpg" rel="10.09.2021-4"><img alt="6-ав.jpg" src="/upload/medialibrary/7df/7dffa940c6ffa5e5547d1624459e2b02.jpg" title="6-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/fcb/fcb54058816b17c591bba22c7e90ad73.jpg" rel="10.09.2021-4"><img alt="6-ухо.jpg" src="/upload/medialibrary/fcb/fcb54058816b17c591bba22c7e90ad73.jpg" title="6-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/fbd/fbdeae8160c6063157628dd18070fc20.jpg" rel="10.09.2021-4"><img alt="6-рв.jpg" src="/upload/medialibrary/fbd/fbdeae8160c6063157628dd18070fc20.jpg" title="6-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>10 гр.</b> диаметр-<b>27,01 мм.</b></p><p style="text-align: center;">Аверс: Вензель «А» на погоне Императора Николая II читается. Крест на
шапке Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается
с пунктира им же и заканчивается.</p>
<p style="text-align: center;">Реверс: надпись со сдвигом, буквы без наклона.</p>

<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/e66/e6603d1af85731177e5bb2bd077df4e6.jpg" rel="10.09.2021-5"><img alt="7-ав.jpg" src="/upload/medialibrary/e66/e6603d1af85731177e5bb2bd077df4e6.jpg" title="7-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/cc3/cc31683e246283423b3b76d44147b6be.jpg" rel="10.09.2021-5"><img alt="7-ухо.jpg" src="/upload/medialibrary/cc3/cc31683e246283423b3b76d44147b6be.jpg" title="7-ухо.jpg" width="174" height="175" align="middle"></a><a href="/upload/medialibrary/b76/b76c87a97456da52dc6f0477f2941f32.jpg" rel="10.09.2021-5"><img alt="7-рв.jpg" src="/upload/medialibrary/b76/b76c87a97456da52dc6f0477f2941f32.jpg" title="7-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>11,2 гр.</b> диаметр-<b>27,84 мм.</b></p>
<p style="text-align: center;">Аверс: Вензель «А» на погоне Императора Николая II читается. Крест на
шапке Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается
с пунктира им же и заканчивается.<br>

Реверс: В написании «1913» цифра «3» с наклоном влево. Надпись со
сдвигом.</p>

<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/907/9078aaca58b67a0a04ab8added3b893e.jpg" rel="10.09.2021-6"><img alt="8-ав.jpg" src="/upload/medialibrary/907/9078aaca58b67a0a04ab8added3b893e.jpg" title="8-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/838/838a200f814e7f76aa23b9b5d2cc9435.jpg" rel="10.09.2021-6"><img alt="8-ухо.jpg" src="/upload/medialibrary/838/838a200f814e7f76aa23b9b5d2cc9435.jpg" title="8-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/629/629ab806a41c85e601db71cc82ae1d73.jpg" rel="10.09.2021-6"><img alt="8-рв.jpg" src="/upload/medialibrary/629/629ab806a41c85e601db71cc82ae1d73.jpg" title="8-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>12,2 гр.</b> диаметр-<b>27,84 мм.</b></p>
<p>Вензель «А» на погоне Императора Николая II читается. Крест на шапке
Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается с пунктира
им же и заканчивается.</p>

<p> </p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/acc/acce984b391ffadaf217a1aaaf063f0e.jpg" rel="14.09.2021-1"><img alt="медь-ав.jpg" src="/upload/medialibrary/acc/acce984b391ffadaf217a1aaaf063f0e.jpg" title="медь-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/aa9/aa9b170f528c2d035e4be4dd45cff222.jpg" rel="14.09.2021-1"><img alt="медь-ухо.jpg" src="/upload/medialibrary/aa9/aa9b170f528c2d035e4be4dd45cff222.jpg" title="медь-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/ee8/ee891a0e77f8675449851473a1a2ef4c.jpg" rel="14.09.2021-1"><img alt="медь-рв.jpg" src="/upload/medialibrary/ee8/ee891a0e77f8675449851473a1a2ef4c.jpg" title="медь-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>13,4 гр.</b> диаметр-<b>28,25 мм</b>.</p>
<div style="text-align: center;">Медаль выполнена из меди.</div><br>
<p>Вензель «А» на погоне Императора Николая II , Крест на шапке Михаила не 
касается сплошной черты. Пунктирная линия на аверсе начинается с точки и
 заканчивается пунктиром. Точки выполнены в виде укороченного пунктира.</p>

<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/474/4746a02657490ae7fbd023c62c31ca63.jpg" rel="15.09.2021-1"><img alt="13-ав.jpg" src="/upload/medialibrary/474/4746a02657490ae7fbd023c62c31ca63.jpg" title="13-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/29e/29ef5eb935975a009edc8a3417444c4b.jpg" rel="15.09.2021-1"><img alt="13-ухо.jpg" src="/upload/medialibrary/29e/29ef5eb935975a009edc8a3417444c4b.jpg" title="13-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/936/9362e4e3dbbd09fb1f47636b5c7d8d0b.jpg" rel="15.09.2021-1"><img alt="13-рв.jpg" src="/upload/medialibrary/936/9362e4e3dbbd09fb1f47636b5c7d8d0b.jpg" title="13-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>13,1 гр.</b> диаметр-<b>28,11 мм.</b></p>
<p style="text-align: center;">Медаль выполнена из серебра.<br>Вензель «А» на погоне Императора Николая II хорошо читается, Крест на
шапке Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается
с пунктира им же и заканчивается. На
обрезе инициалы «МШ».</p>

<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/7f7/7f7aa2f0aae5b5374fb23c267afb37a6.jpg" rel="15.09.2021-2"><img alt="10 ав.jpg" src="/upload/medialibrary/7f7/7f7aa2f0aae5b5374fb23c267afb37a6.jpg" title="10 ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/4e0/4e02ed26261b89d9f3007c39af150f52.jpg" rel="15.09.2021-2"><img alt="10-ухо.jpg" src="/upload/medialibrary/4e0/4e02ed26261b89d9f3007c39af150f52.jpg" title="10-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/08c/08c4a6b3dc8fead8cff02f7bc26b8ac7.jpg" rel="15.09.2021-2"><img alt="10-рв.jpg" src="/upload/medialibrary/08c/08c4a6b3dc8fead8cff02f7bc26b8ac7.jpg" title="10-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>15,5 гр.</b> диаметр-<b>28,95 мм.<br></b></p>
<p>Вензель на погоне Императора Николая II неразборчив, Крест на шапке
Михаила касается сплошной черты. Пунктирная линия на аверсе начинается с
пунктира им же и заканчивается. </p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/5a3/5a312fe15338aebd7fc62250349d4360.jpg" rel="15.09.2021-03"><img alt="9-ав.jpg" src="/upload/medialibrary/5a3/5a312fe15338aebd7fc62250349d4360.jpg" title="9-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/093/0934bc3ce9980806320c5dbf536e9772.jpg" rel="15.09.2021-03"><img alt="9-ухо.jpg" src="/upload/medialibrary/093/0934bc3ce9980806320c5dbf536e9772.jpg" title="9-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/782/782ac4cd23d39131d23ca6bff552df68.jpg" rel="15.09.2021-03"><img alt="9-рв.jpg" src="/upload/medialibrary/782/782ac4cd23d39131d23ca6bff552df68.jpg" title="9-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>12,8 гр.</b> диаметр-<b>28,35 мм.</b></p><p style="text-align: center;"></p><p style="text-align: center;">Золочение. Вензель «А» на погоне Императора Николая II, Крест на шапке Михаила не
касается сплошной черты. Пунктирная линия на аверсе начинается с точки и
заканчивается штрихом.</p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/8e7/8e7840160c8efafe190d6f420b105c2a.jpg" rel="15.09.2021-04"><img alt="11-ав.jpg" src="/upload/medialibrary/8e7/8e7840160c8efafe190d6f420b105c2a.jpg" title="11-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/b74/b74b9446984cf891ff0510daf65a5062.jpg" rel="15.09.2021-04"><img alt="11-ухо.jpg" src="/upload/medialibrary/b74/b74b9446984cf891ff0510daf65a5062.jpg" title="11-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/9de/9de07bfa7fdbfb7d60e4ff9336d19069.jpg" rel="15.09.2021-04"><img alt="11-рв.jpg" src="/upload/medialibrary/9de/9de07bfa7fdbfb7d60e4ff9336d19069.jpg" title="11-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>12,2 гр.</b> диаметр-<b>28,31 мм.</b></p>
<div>Вензель на погоне Императора Николая II неразборчив, Крест на шапке
Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается с
пунктира им же и заканчивается. <hr></div>
<p style="text-align: center;"><a href="/upload/medialibrary/7aa/7aa33c37267645b3856a574c7073ce5e.jpg" rel="15.9.2021-5"><img alt="12-ав.jpg" src="/upload/medialibrary/7aa/7aa33c37267645b3856a574c7073ce5e.jpg" title="12-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/d48/d48ae218ba3a14326a8e3485dfc59679.jpg" rel="15.9.2021-5"><img alt="12-ухо.jpg" src="/upload/medialibrary/d48/d48ae218ba3a14326a8e3485dfc59679.jpg" title="12-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/82e/82e9977fd7c1da49e24e094a756ec776.jpg" rel="15.9.2021-5"><img alt="12-рв.jpg" src="/upload/medialibrary/82e/82e9977fd7c1da49e24e094a756ec776.jpg" title="12-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>12,7 гр.</b> диаметр-<b>28,71 мм.</b></p>
<div>Золочение. Вензель «А» на погоне Императора Николая II, Крест на шапке Михаила не
касается сплошной черты. Пунктирная линия на аверсе начинается с пунктира и
заканчивается точкой.  На обрезе инициалы
«МШ».
<hr> <br></div>
<p style="text-align: center;"><a href="/upload/medialibrary/05f/05fe482f933ec8cec8d18af2a3c8b183.jpg" rel="15.09.2021-06"><img alt="14-ав.jpg" src="/upload/medialibrary/05f/05fe482f933ec8cec8d18af2a3c8b183.jpg" title="14-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/229/22910a6d4ddaf90b6b9b8f61c6464672.jpg" rel="15.09.2021-06"><img alt="14-ухо.jpg" src="/upload/medialibrary/229/22910a6d4ddaf90b6b9b8f61c6464672.jpg" title="14-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/105/1051021c8bdcb070491609094336fe3b.jpg" rel="15.09.2021-06"><img alt="14-рв.jpg" src="/upload/medialibrary/105/1051021c8bdcb070491609094336fe3b.jpg" title="14-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>9,2 гр.</b> диаметр-<b>27,85 мм.</b></p><p></p><p>Вензель «S» на
погоне Императора Николая II хорошо читается, Крест на шапке Михаила не
касается сплошной черты. Пунктирная линия на аверсе начинается с пунктира и
заканчивается точкой. 

Буквы реверса с наклоном и сдвигом вправо.</p>

<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/a79/a79d2f7d11b12f68abef39594b3c20da.jpg" rel="15.сент.2021-15"><img alt="15-ав.jpg" src="/upload/medialibrary/a79/a79d2f7d11b12f68abef39594b3c20da.jpg" title="15-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/939/9394aa1925a9c90f5b317bd9641f217c.jpg" rel="15.сент.2021-15"><img alt="15-ухо.jpg" src="/upload/medialibrary/939/9394aa1925a9c90f5b317bd9641f217c.jpg" title="15-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/f0f/f0f9e4aa218ff7831ee57a9f513f3546.jpg" rel="15.сент.2021-15"><img alt="15-рв.jpg" src="/upload/medialibrary/f0f/f0f9e4aa218ff7831ee57a9f513f3546.jpg" title="15-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>9,7 гр.</b> диаметр-<b>27,85 мм.</b></p><div>Позолота. Вензель «S» на
погоне Императора Николая II хорошо читается, Крест на шапке Михаила не
касается сплошной черты. Пунктирная линия на аверсе начинается с пунктира им же
и заканчивается.Буквы реверса с наклоном и сдвигом вправо.
 <br><hr>
<p style="text-align: center;"><a href="/upload/medialibrary/02f/02febf7da50a0ac0852264ef774cc14b.jpg" rel="16.09.2021-1"><img alt="16-ав.jpg" src="/upload/medialibrary/02f/02febf7da50a0ac0852264ef774cc14b.jpg" title="16-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/80a/80a889e67539febbeeb7c1033ae693cc.jpg" rel="16.09.2021-1"><img alt="16-ухо.jpg" src="/upload/medialibrary/80a/80a889e67539febbeeb7c1033ae693cc.jpg" title="16-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/f38/f384b2e1d9e1fb7019b98e2c0e78377d.jpg" rel="16.09.2021-1"><img alt="16-рв.jpg" src="/upload/medialibrary/f38/f384b2e1d9e1fb7019b98e2c0e78377d.jpg" title="16-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>11,92 гр.</b> диаметр-<b>28,34 мм.</b></p><p></p>
<div>Золочение. Предположительно отчеканена пескоструйным штампом.</div>

<br>Вензель «А» на погоне Императора Николая II, Крест на шапке Михаила
касается сплошной черты. Отсутствует православный крест на шее Михаила. Пунктирная
линия на аверсе начинается с точки ею же и заканчивается.
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/af9/af9cd0e46e6f6171717e61d71e8b29e3.jpg" rel="16.09.2021-2"><img alt="17-ав.jpg" src="/upload/medialibrary/af9/af9cd0e46e6f6171717e61d71e8b29e3.jpg" title="17-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/f0e/f0e4f956bf36f83453d08664e7eea351.jpg" rel="16.09.2021-2"><img alt="17-ухо.jpg" src="/upload/medialibrary/f0e/f0e4f956bf36f83453d08664e7eea351.jpg" title="17-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/690/690ee551af0195c0b895f19bc7adf7f4.jpg" rel="16.09.2021-2"><img alt="17-рв.jpg" src="/upload/medialibrary/690/690ee551af0195c0b895f19bc7adf7f4.jpg" title="17-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>11,62 гр.</b> диаметр-<b>28,39 мм.<br></b></p><p style="text-align: center;"></p><p style="text-align: center;">

</p><p>Золочение. Вензель «А» на погоне Императора Николая II, Крест на шапке Михаила не
касается сплошной черты. Отсутствует православный крест на шее Михаила.
Пунктирная линия на аверсе начинается с точки и заканчивается штрихом. Надпись
на реверсе смещена вправо.


</p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/e40/e40bcdb0e15569d4dcdb826d2cbce215.jpg" rel="16.09.2021-3"><img alt="18-ав.jpg" src="/upload/medialibrary/e40/e40bcdb0e15569d4dcdb826d2cbce215.jpg" title="18-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/cbe/cbeb9b1922d8804faf7c5479df79bafb.jpg" rel="16.09.2021-3"><img alt="18-ухо.jpg" src="/upload/medialibrary/cbe/cbeb9b1922d8804faf7c5479df79bafb.jpg" title="18-ухо.jpg" width="174" height="175" align="middle"></a><a href="/upload/medialibrary/3f7/3f75b7bdc00175b595272cd61db14dbd.jpg" rel="16.09.2021-3"><img alt="18-рв.jpg" src="/upload/medialibrary/3f7/3f75b7bdc00175b595272cd61db14dbd.jpg" title="18-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>13,32 гр.</b> диаметр-<b>28,32 мм.</b></p><p></p><div>Золочение.

Вензель «А» на погоне казачьего типа Императора Николая II, Крест на
шапке Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается
с пунктира им же и заканчивается. Отсутствие глазных яблок в глазницах.</div>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/cc8/cc8fcdac91450e891849b075f53608bc.jpg" rel="16.09.2021-04"><img alt="19-ав.jpg" src="/upload/medialibrary/cc8/cc8fcdac91450e891849b075f53608bc.jpg" title="19-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/951/951d7583a9ab61602d90e01c1b2a25f7.jpg" rel="16.09.2021-04"><img alt="19-ухо.jpg" src="/upload/medialibrary/951/951d7583a9ab61602d90e01c1b2a25f7.jpg" title="19-ухо.jpg" width="175" height="166" align="middle"></a><a href="/upload/medialibrary/a84/a847f6384a6c38bf3b28fc3c12b814d9.jpg" rel="16.09.2021-04"><img alt="19-рв.jpg" src="/upload/medialibrary/a84/a847f6384a6c38bf3b28fc3c12b814d9.jpg" title="19-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>12,82 гр.</b> диаметр-<b>28,27 мм.</b></p>
<p>Золочение. Вензель «А» на погоне Императора Николая II, Крест на шапке Михаила
касается сплошной черты. Православный крест на шее Михаила отсутствует. Пунктирная линия на аверсе начинается с
пунктира им же и заканчивается.
 <br>
</p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/6ff/6ff2016416dfa73e4e111fe766f64169.jpg" rel="16сент-20"><img alt="20-ав.jpg" src="/upload/medialibrary/6ff/6ff2016416dfa73e4e111fe766f64169.jpg" title="20-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/1d5/1d57394cf67e5efe06bd05aa91a17048.jpg" rel="16сент-20"><img alt="20-ухо.jpg" src="/upload/medialibrary/1d5/1d57394cf67e5efe06bd05aa91a17048.jpg" title="20-ухо.jpg" width="174" height="175" align="middle"></a><a href="/upload/medialibrary/165/16530a53b13b44a4e6a4be1088814b17.jpg" rel="16сент-20"><img alt="20-рв.jpg" src="/upload/medialibrary/165/16530a53b13b44a4e6a4be1088814b17.jpg" title="20-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>12,22 гр.</b> диаметр-<b>28,44 мм.</b></p><div>Следы золочения. Вензель «А» на погоне Императора Николая II, Крест на шапке Михаила
касается сплошной черты. Православный крест на шее Михаила отсутствует. Пунктирная линия на аверсе начинается с точки
ею же и заканчивается. 

<br>
<p></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/5a0/5a08d0290b56abf1564e215763c4038b.jpg" rel="16-09-21a"><img alt="21-ав.jpg" src="/upload/medialibrary/5a0/5a08d0290b56abf1564e215763c4038b.jpg" title="21-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/b64/b64cd6feb2a203469ae253d7ad53502c.jpg" rel="16-09-21a" title="21-рв.jpg"><img alt="21-ухо.jpg" src="/upload/medialibrary/b64/b64cd6feb2a203469ae253d7ad53502c.jpg" title="21-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary//027/0278ec17c081e0f33846fc83eb9aef6c.jpg" rel="16-09-21a" title="21-рв.jpg"><img alt="21-рв.jpg" src="/upload/medialibrary/027/0278ec17c081e0f33846fc83eb9aef6c.jpg" title="21-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>10,11 гр.</b> диаметр-<b>27,94 мм.</b></p>
<p></p><div>Золочение. Вензель «S»с
короной на погоне Императора Николая II, Крест на шапке Михаила касается
сплошной черты. </div>Православный крест на шее Михаила отсутствует. Пунктирная линия на аверсе начинается с
пунктира и заканчивается точкой. 

<br>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/c4e/c4e644e98f3899ece982d4aca9c6a6ad.jpg" rel="16.09-22"><img alt="22-ав.jpg" src="/upload/medialibrary/c4e/c4e644e98f3899ece982d4aca9c6a6ad.jpg" title="22-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/aaa/aaaf56f3ca9fc3aba38fb00dc8e246c8.jpg" rel="16.09-22"><img alt="22-ухо.jpg" src="/upload/medialibrary/aaa/aaaf56f3ca9fc3aba38fb00dc8e246c8.jpg" title="22-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/1b0/1b01ce966c2756875591a733cbbf9bac.jpg" rel="16.09-22"><img alt="22-рв.jpg" src="/upload/medialibrary/1b0/1b01ce966c2756875591a733cbbf9bac.jpg" title="22-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;">Вес-<b>11,21 гр.</b> диаметр-<b>28,31 мм.</b></p><p>Золочение. Вензель «А» на погоне Императора Николая II, Крест на шапке Михаила не
касается сплошной черты. Наличие Православного креста на шее Михаила. Пунктирная линия на аверсе начинается с точки
и заканчивается пунктиром. 

</p>

<hr>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>