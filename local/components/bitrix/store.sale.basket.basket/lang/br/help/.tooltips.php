<?
$MESS["PATH_TO_ORDER_TIP"] = "Nome do caminho da pбgina de conclusгo do pedido. Vocк pode especificar somente o nome do arquivo se a pбgina estiver no diretуrio atual. ";
$MESS["HIDE_COUPON_TIP"] = "Selecione \"Sim\" para ocultar o campo de entrada do cуdigo do cupom na pбgina do carrinho. ";
$MESS["COLUMNS_LIST_TIP"] = "Os campos selecionados serгo utilizados como tнtulos das colunas na tabela de conteъdos do carrinho. ";
$MESS["SET_TITLE_TIP"] = "Ativar esta opзгo modificarб o tнtulo da pбgina para \"Meu carrinho de compras\". ";
$MESS["QUANTITY_FLOAT_TIP"] = "Ative esta opзгo para autorizar quantidades fracionadas de itens.";
$MESS["PRICE_VAT_INCLUDE_TIP"] = "Ativar esta opзгo significa incluir impostos nos preзos exibidos.";
$MESS["PRICE_VAT_SHOW_VALUE_TIP"] = "Define a exibiзгo do valor do imposto.";
?>