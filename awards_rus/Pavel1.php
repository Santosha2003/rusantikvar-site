<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Павел I");
?><div style="background-color: white">
<h2 style="text-align: center;"> <b>Период царствования Павла I <br>
 </b></h2>
			<h2 style="text-align: center;"><b>(1796-1801)</b> </h2>
	<div>
 <b> </b>
<!--margin-left="auto" style="display: flex; justify-content: center;" class="margin-right: auto; margin-left: auto;"  target="_blank"--> <!--style="outline-width: 0px !important; user-select: auto !important;" --> <br></div><p style="text-align: center;"></p><br><p><br></p><p style="text-align: center;">
 <a href="/upload/medialibrary/641/64115181a134b845d319c01ec2b788ba.jpg" rel="images1"><img alt="peter1" src="/upload/medialibrary/641/64115181a134b845d319c01ec2b788ba.jpg" title="peter1" rel="images1" href="/upload/medialibrary/641/64115181a134b845d319c01ec2b788ba.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/208/2088b1b6564d2c222474f3517b530e10.jpg" rel="images1"><img alt="Зоса-4112-ухо.jpg" src="/upload/medialibrary/208/2088b1b6564d2c222474f3517b530e10.jpg" title="Зоса-4112-ухо.jpg" width="180" height="180" align="middle"></a> <a href="/upload/medialibrary/80e/80ec114a6754d6845e4c5101001b334f.jpg" rel="images1"><img alt="peter2" src="/upload/medialibrary/80e/80ec114a6754d6845e4c5101001b334f.jpg" rel="images1" title="peter2" href="/upload/medialibrary/80e/80ec114a6754d6845e4c5101001b334f.jpg" width="180" height="180" align="middle"></a>
</p>
 <!--<hr><p style="text-align: center;"> <a  margin-left="auto" rel="images2" href="/upload/medialibrary/76b/76b202b22f3f3188779b7e216245638b.jpg" style="outline-width: 0px !important; user-select: auto !important;"></a> </p> .. 180x180 3 pictures --> <br>
<p style="text-align: justify;">
 <b>&nbsp;&nbsp; Знак отличия ордена Святой Анны №4.112 </b>СПб монетный двор, серебро 72 пробы, вес 7,5 гр., размеры 29,4х23,3 мм.,толщина 2,47 мм., корона у основания 10 мм., ухо у основания 5,81 мм.&nbsp;<br>
	 &nbsp;&nbsp; Знаком отличия за №4.112 награжден<b> <span style="color: #000000;">Алексей Аксенов</span></b> – рядовой, Владимирского Гарнизонного Шурманова батальона. В графе «Возвращение знаков, изменения» карандашом написано – умер. ШУРМАН Леонтий Иванович – подполковник (с 14.07.1797 полковник) 09.01.1797–23.02.1798 – шеф Владимирского гарнизонного батальона. РГИА, 496 фонд, 3 опись, 282 дело, 73 стр. &nbsp;&nbsp;&nbsp;&nbsp; <br>
	 &nbsp; &nbsp; Изображение предоставил <b>А.Б.</b></p>
<hr>
<p style="text-align: center;"><b><a href="/upload/medialibrary/14a/14a74d723c8c8d63032674552b0a1710.jpg" rel="1-14-08-2020"><img alt="ЗООСА-8.434-ав.jpg" src="/upload/medialibrary/14a/14a74d723c8c8d63032674552b0a1710.jpg" title="ЗООСА-8.434-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/ac8/ac87429b2e226ccc191a396b8e2473a6.jpg" rel="1-14-08-2020"><img alt="ЗООСА-8.434-ухо.jpg" src="/upload/medialibrary/ac8/ac87429b2e226ccc191a396b8e2473a6.jpg" title="ЗООСА-8.434-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/7a1/7a1bf5133d7ebf869ba2a8c6c3b75f02.jpg" rel="1-14-08-2020"><img alt="ЗООСА-8.434-рв.jpg" src="/upload/medialibrary/7a1/7a1bf5133d7ebf869ba2a8c6c3b75f02.jpg" title="ЗООСА-8.434-рв.jpg" width="185" height="185" align="middle"></a><br></b></p><p style="text-align: center;"><br><b>&nbsp; Знак отличия ордена Святой Анны №8.434 </b>СПб монетный двор, 
серебро 72 пробы, вес 7,51 гр., размеры 30х23,11 мм.,толщина 2,11 мм., 
корона у основания 9,7 мм., ухо у основания 6,0 мм.
</p>
<hr>
<p style="text-align: center;">
 <b><a href="/upload/medialibrary/586/5860e941195ff3ecf3159a69aef1f614.jpg" rel="images30-04-2020"><img alt="ЗООСА-16.609-ав.jpg" src="/upload/medialibrary/586/5860e941195ff3ecf3159a69aef1f614.jpg" title="ЗООСА-16.609-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/5f1/5f1ee684d563b54cabc66f93d9700ba3.jpg" rel="images30-04-2020"><img alt="ЗООСА-16.609-ухо.jpg" src="/upload/medialibrary/5f1/5f1ee684d563b54cabc66f93d9700ba3.jpg" title="ЗООСА-16.609-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/bbc/bbcb8ca3ae13d3d0a2d04fc6cb94b401.jpg" rel="images30-04-2020"><img alt="ЗООСА-16.609-рв.jpg" src="/upload/medialibrary/bbc/bbcb8ca3ae13d3d0a2d04fc6cb94b401.jpg" title="ЗООСА-16.609-рв.jpg" width="185" height="185" align="middle"></a><br>
 </b>
</p>
<p style="text-align: center;">
 <br>
 <b> <b>Знак отличия ордена Святой Анны №16.609</b> </b>СПб монетный двор, серебро 72 пробы, вес 8,5 гр., толщина 2,13 мм., размер 30,4х23,1 мм., ухо 5,42 мм., корона 9,9 мм. <b><br>
 </b>
</p>
<p style="text-align: center;">
	 Знаком отличия за №16.609 награжден <b>Семен Саматов</b> – унтер-офицер, 1-го Гарнизонного мушкетерского генерал-майора и кавалера князя Мещерского полка в 1798 г. <br>
</p>
<p style="text-align: left;">
	 Изображение предоставил <b>Сергей Трофимов.</b>
</p>
<hr>
<p style="text-align: center;">
 <a rel="group2" href="/upload/medialibrary/766/76660e4d0a0ac9d17d3b86d2e35c3e08.jpg"><img alt="peter3" src="/upload/medialibrary/766/76660e4d0a0ac9d17d3b86d2e35c3e08.jpg" title="peter3" rel="group2" width="180" height="180" align="middle"></a><a href="http://rusantikvar.ru/upload/medialibrary/01b/01ba4734d6833eae3027c50b4028340e.jpg" rel="group2"><img alt="ЗООСА-20.750-ухо.jpg" src="/upload/medialibrary/01b/01ba4734d6833eae3027c50b4028340e.jpg" title="ЗООСА-20.750-ухо.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/76b/76b202b22f3f3188779b7e216245638b.jpg" rel="group2"><img alt="peter4" src="/upload/medialibrary/76b/76b202b22f3f3188779b7e216245638b.jpg" title="peter4" width="180" height="180" align="middle"></a>
</p>
 <b> </b>
<div>
	 <!-- этот блок сверху от <p> до </p> настроен из админки - размер картинки 180 точек , выравнивание по центру у всех трех,
 ссылка на изображении - копировать первую ссылку на картинку и вставить, rel чтобы правильно работала карусель на яве - у всех трех картинок одинаковый images3 --> <b>&nbsp;&nbsp;&nbsp;&nbsp; <br>
 </b>
</div>
<p style="text-align: justify;">
	 &nbsp;&nbsp;&nbsp; <b>Знак отличия ордена Святой Анны №20.750</b> СПб монетный двор, серебро 72 пробы, вес 8,5 гр., толщина 2,13 мм., размер 30,4х23,1 мм., ухо 5,41 мм., корона 10 мм. <br>
</p>
<p style="text-align: justify;">
	 Изображение предоставил <b>ФИНВАЛ.</b></p>
<hr>
<p style="text-align: center;"><b><a href="/upload/medialibrary/941/94184e67286ee6d3f0c9b8d0be21737a.jpg" rel="19.08.2021-1"><img alt="ЗООСА-35922-ав.jpg" src="/upload/medialibrary/941/94184e67286ee6d3f0c9b8d0be21737a.jpg" title="ЗООСА-35922-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/870/87016f1a7c00ce72d101455fa964ae62.jpg" rel="19.08.2021-1"><img alt="ЗООСА-35922-ухо.jpg" src="/upload/medialibrary/870/87016f1a7c00ce72d101455fa964ae62.jpg" title="ЗООСА-35922-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/833/833c995a3083bd54037ab23bc2eda67c.jpg" rel="19.08.2021-1"><img alt="ЗООСА-35922-рв.jpg" src="/upload/medialibrary/833/833c995a3083bd54037ab23bc2eda67c.jpg" title="ЗООСА-35922-рв.jpg" width="185" height="185" align="middle"></a><br></b></p><br><div><b>Знак отличия ордена Святой Анны №35.922</b> СПб монетный двор, серебро 72 пробы, вес 7,6 гр., толщина 1,98 мм., размер 29,42х22,93 мм., ухо 5,54 мм., корона 9,94 мм.</div><br>

Изображение предоставил <b>А.Б.</b><hr>
<h2></h2>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>