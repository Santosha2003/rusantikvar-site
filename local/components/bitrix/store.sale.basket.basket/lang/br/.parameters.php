<?
$MESS["SBB_DESC_YES"] = "Sim";
$MESS["SBB_DESC_NO"] = "Nгo";
$MESS["SBB_PATH_TO_ORDER"] = "Pбgina do pedido";
$MESS["SBB_HIDE_COUPON"] = "Ocultar cupom de entrada";
$MESS["SBB_COLUMNS_LIST"] = "Colunas";
$MESS["SBB_BNAME"] = "Nome do produto";
$MESS["SBB_BPROPS"] = "Propriedades do produto";
$MESS["SBB_BPRICE"] = "Preзo";
$MESS["SBB_BTYPE"] = "Tipo de preзo";
$MESS["SBB_BQUANTITY"] = "Quantidade";
$MESS["SBB_BDELETE"] = "Deletar";
$MESS["SBB_BDELAY"] = "Pausar";
$MESS["SBB_BWEIGHT"] = "Peso";
$MESS["SBB_BDISCOUNT"] = "Desconto";
$MESS["SBB_WEIGHT_UNIT"] = "Unidade de peso";
$MESS["SBB_WEIGHT_UNIT_G"] = "g";
$MESS["SBB_WEIGHT_KOEF"] = "Gramas na unidade de peso";
$MESS["SBB_QUANTITY_FLOAT"] = "Permitir uso de valores fracionados para quantidade";
$MESS["SBB_VAT_SHOW_VALUE"] = "Exibir valor da taxa de impostos";
$MESS["SBB_VAT_INCLUDE"] = "Incluir impostos no preзo";
$MESS["SBB_COUNT_DISCOUNT_4_ALL_QUANTITY"] = "Calcular desconto para cada produto do pedido (quantidade total)";
$MESS["SBB_USE_PREPAYMENT"] = "Utilizar checkout expresso PayPal";
$MESS["SBB_BSUM"] = "Total";
?>