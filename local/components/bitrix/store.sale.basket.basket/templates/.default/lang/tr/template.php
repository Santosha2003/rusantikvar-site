<?
$MESS ['SALE_REFRESH'] = "Yenile       ";
$MESS ['SALE_ORDER'] = "Sipariюi dьzenle";
$MESS ['SALE_NAME'] = "Ad";
$MESS ['SALE_PRICE'] = "Fiyat";
$MESS ['SALE_PRICE_TYPE'] = "Fiyat tьrь";
$MESS ['SALE_QUANTITY'] = "Sayэ";
$MESS ['SALE_DELETE'] = "Sil";
$MESS ['SALE_OTLOG'] = "Beklet";
$MESS ['SALE_WEIGHT'] = "Aрэrlэk";
$MESS ['SALE_WEIGHT_G'] = "g";
$MESS ['SALE_ITOGO'] = "Toplam";
$MESS ['SALE_REFRESH_DESCR'] = "Ьrьnleri yeniden hesaplamak, silme veya bekletmek iзin bu dьрmeye tэklatэnэz.";
$MESS ['SALE_ORDER_DESCR'] = "Sepette bulunan ьrьnleri sipariю etmek iзin bu dьрmeye tэklatэnэz";
$MESS ['SALE_OTLOG_TITLE'] = "Bekletiliyor";
$MESS ['SALE_UNAVAIL_TITLE'] = "Satэюta yok";
$MESS ['STB_ORDER_PROMT'] = "Sipariюin dьzenlenmesini baюlatmak iзin, \"Sipariю dьzenle\" dьрmesine tэklatэnэz.";
$MESS ['STB_COUPON_PROMT'] = "Эndirimden yararlanmak iзin kupon kodunuz var ise, buraya giriniz:";
$MESS ['SALE_VAT'] = "KDV:";
$MESS ['SALE_VAT_INCLUDED'] = "KDV dahil:";
$MESS ['SALE_TOTAL'] = "Toplam:";
$MESS ['SALE_CONTENT_DISCOUNT'] = "Эndirim";
$MESS ['SALE_DISCOUNT'] = "Эndirim";
?>