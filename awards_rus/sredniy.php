<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Средний рельеф");
?><p style="text-align: center;">
 <b><span style="font-size: 15pt;">Средний рельеф.</span></b>
</p>
<div style="background-color: white">
<p style="text-align: center;">
 <b><span style="font-size: 15pt;"><a href="/upload/medialibrary/c7f/c7f7a1514fc604604073bbdcf5092af2.jpg" rel="gr0409"><img alt="4-средн.рельеф-аверс.jpg" src="/upload/medialibrary/c7f/c7f7a1514fc604604073bbdcf5092af2.jpg" title="4-средн.рельеф-аверс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/b82/b823c19c90f7015da52afa7b02dbc89d.jpg" rel="gr0409"><img alt="4-средн.рельеф-ухо.jpg" src="/upload/medialibrary/b82/b823c19c90f7015da52afa7b02dbc89d.jpg" title="4-средн.рельеф-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/124/124b877c3a39d00a3ac60f76c55ac0c9.jpg" rel="gr0409"><img alt="4-средн.рельеф-реверс.jpg" src="/upload/medialibrary/124/124b877c3a39d00a3ac60f76c55ac0c9.jpg" title="4-средн.рельеф-реверс.jpg" width="185" height="185" align="middle"></a><br>
 </span></b>
</p>
<p style="text-align: center;">
 <br>
	 Вес-<b>13,6 гр.</b> диаметр-<b>28,5 мм.</b>
</p>
<p style="text-align: left;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель "А" на погоне Императора Николая II не четко выражен, крест на шапке Михаила не касается сплошной черты.<br>
	 Пунктирная линия на аверсе начинается с точки и точкой заканчивается. На реверсе смещение букв. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/7c9/7c9ca614a10c433189b14528d457d32e.jpg" rel="gr05091"><img alt="средний-рельеф-6-аверс.jpg" src="/upload/medialibrary/7c9/7c9ca614a10c433189b14528d457d32e.jpg" title="средний-рельеф-6-аверс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/582/5825412fac8ad5c323df0300fd59c40d.jpg" rel="gr05091"><img alt="средний-рельеф-6-ухо.jpg" src="/upload/medialibrary/582/5825412fac8ad5c323df0300fd59c40d.jpg" title="средний-рельеф-6-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/dfe/dfe42c849e377ec7352d255845883fa8.jpg" rel="gr05091"><img alt="средний-рельеф-6-реверс.jpg" src="/upload/medialibrary/dfe/dfe42c849e377ec7352d255845883fa8.jpg" title="средний-рельеф-6-реверс.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>10,3 гр.</b> диаметр-<b>28,4 мм. <br>
 </b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp; Вензель "А" на погоне Императора Николая II прописной и четко выражен, крест на шапке Михаила не касается сплошной черты.<br>
	 Пунктирная линия на аверсе начинается с точки и заканчивается чертой. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/b21/b216b2fb959958f152d121afc4e6d59e.jpg" rel="gr05092"><img alt="вариант-8-аверс.jpg" src="/upload/medialibrary/b21/b216b2fb959958f152d121afc4e6d59e.jpg" title="вариант-8-аверс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/aaf/aafb8a0d4bd80f343a39deafb75ac12a.jpg" rel="gr05092"><img alt="вариант-8-ухо.jpg" src="/upload/medialibrary/aaf/aafb8a0d4bd80f343a39deafb75ac12a.jpg" title="вариант-8-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/f4b/f4bf504a96d43aab45e2c479f1fc5edd.jpg" rel="gr05092"><img alt="вариант-8-реверс.jpg" src="/upload/medialibrary/f4b/f4bf504a96d43aab45e2c479f1fc5edd.jpg" title="вариант-8-реверс.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>11,1 гр.</b> диаметр-<b>27,6 мм. <br>
 </b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель "S" на погоне Императора Николая II не четко выражен, крест на шапке Михаила касается черты.<br>
	 Пунктирная линия на аверсе начинается с пунктирной линией и заканчивается точкой.Медаль с серебрением и золочением. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/d87/d87ce4d774644f7da4a90386290df710.jpg" rel="gr05093"><img alt="Вариант-10-аверс-.jpg" src="/upload/medialibrary/d87/d87ce4d774644f7da4a90386290df710.jpg" title="Вариант-10-аверс-.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/ba1/ba1eb6d038548e7aa64a5447b5bffa61.jpg" rel="gr05093"><img alt="Вариант-10-ухо.jpg" src="/upload/medialibrary/ba1/ba1eb6d038548e7aa64a5447b5bffa61.jpg" title="Вариант-10-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/73b/73b40d72f5accddea1d6744cc6f67efb.jpg" rel="gr05093"><img alt="Вариант-10-реверс-.jpg" src="/upload/medialibrary/73b/73b40d72f5accddea1d6744cc6f67efb.jpg" title="Вариант-10-реверс-.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>12,9 гр.</b> диаметр-<b>28,3 мм. <br>
 </b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель "А" печатной буквой на погоне Императора Николая II не четко выражен, крест на шапке Михаила касается сплошной черты. Пунктирная линия на аверсе начинается с пунктира и заканчивается пунктиром. Шрифт до ДР меньше, буквы с засечкой. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/c99/c99ef5ae3adb273979eef21231217782.jpg" rel="gr05094"><img alt="сред-рельеф11-аверс.jpg" src="/upload/medialibrary/c99/c99ef5ae3adb273979eef21231217782.jpg" title="сред-рельеф11-аверс.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/cb8/cb8ded45bcfb60eccdf99c5b17d99248.jpg" rel="gr05094"><img alt="сред-рельеф11-ухо.jpg" src="/upload/medialibrary/cb8/cb8ded45bcfb60eccdf99c5b17d99248.jpg" title="сред-рельеф11-ухо.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/89c/89c7c657ddb1e73125dc05f20fed4347.jpg" rel="gr05094"><img alt="сред-рельеф11-реверс.jpg" src="/upload/medialibrary/89c/89c7c657ddb1e73125dc05f20fed4347.jpg" title="сред-рельеф11-реверс.jpg" width="180" height="180" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>12,5 гр.</b> диаметр-<b>28,1 мм. <br>
 </b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель "А" на погоне Императора Николая II печатной буквой, крест на шапке Михаила касается черты. Пунктирная линия на аверсе начинается с пунктирной линией и заканчивается пунктирной линией . Шрифт отличается. Над буквой I нет точки. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/2d8/2d85bdeb6a9f12af4deb2a35b8fbefc3.jpg" rel="gr05095"><img alt="13-аверс.jpg" src="/upload/medialibrary/2d8/2d85bdeb6a9f12af4deb2a35b8fbefc3.jpg" title="13-аверс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/50c/50c96fae86c5bcf1c2a2c42281f57a23.jpg" rel="gr05095"><img alt="13-ухо.jpg" src="/upload/medialibrary/50c/50c96fae86c5bcf1c2a2c42281f57a23.jpg" title="13-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/678/67837a2eadc431e7f1950932dc9afde6.jpg" rel="gr05095"><img alt="13-реверс.jpg" src="/upload/medialibrary/678/67837a2eadc431e7f1950932dc9afde6.jpg" title="13-реверс.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>12,7 гр.</b> диаметр-<b>28,4 мм. <br>
 </b>
</p>
<p style="text-align: left;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель "А" на погоне Императора Николая II не четко выражен, крест на шапке Михаила не касается сплошной черты, орнамент узора на шапке Мономаха точечный. Пунктирная линия на аверсе начинается с точки ею и заканчивается. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/226/226c20047a1320d26657430e232de7ff.jpg" rel="gr050905"><img alt="сред.рельеф15-аверс.jpg" src="/upload/medialibrary/226/226c20047a1320d26657430e232de7ff.jpg" title="сред.рельеф15-аверс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/044/044fbc887941d60c7d18d0c9d9b08ea2.jpg" rel="gr050905"><img alt="сред.рельеф15-ухо.jpg" src="/upload/medialibrary/044/044fbc887941d60c7d18d0c9d9b08ea2.jpg" title="сред.рельеф15-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/2f4/2f48143f980469422d11a6ae029f61dd.jpg" rel="gr050905"><img alt="сред.рельеф15-реверс.jpg" src="/upload/medialibrary/2f4/2f48143f980469422d11a6ae029f61dd.jpg" title="сред.рельеф15-реверс.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>11,4 гр.</b> диаметр-<b>28,3 мм.</b>
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp;&nbsp; Вензель на погоне Императора Николая II "S" латинской буквой, крест на шапке Михаила касается сплошной черты. Пунктирная линия на аверсе начинается с пунктира им же и заканчивается. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/2d9/2d9421cf6a873ce2a11e123ad046d5a7.jpg" rel="gr050906"></a>
</p>
<p style="text-align: center;">
 <a href="/upload/medialibrary/2d9/2d9421cf6a873ce2a11e123ad046d5a7.jpg" rel="gr050906"><img alt="20лицо-и-ухо-ав.jpg" src="/upload/medialibrary/2d9/2d9421cf6a873ce2a11e123ad046d5a7.jpg" title="20лицо-и-ухо-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/0d2/0d2f7e58a3a9b87aa6ec84c0aecace56.jpg" rel="gr050906"><img alt="20лицо-и-ухо-ухо.jpg" src="/upload/medialibrary/0d2/0d2f7e58a3a9b87aa6ec84c0aecace56.jpg" title="20лицо-и-ухо-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/b64/b64e7d3076bb6f3993d85d028a038a9e.jpg" rel="gr050906"><img alt="20лицо-и-ухо-рев.jpg" src="/upload/medialibrary/b64/b64e7d3076bb6f3993d85d028a038a9e.jpg" title="20лицо-и-ухо-рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
 <a href="/upload/medialibrary/b64/b64e7d3076bb6f3993d85d028a038a9e.jpg" rel="gr050906"></a>
</p>
<p>
</p>
<p style="text-align: center;">
	 Вес-<b>10,6 гр.</b> диаметр-<b>28,6 мм. <br>
 </b>
</p>
<p style="text-align: left;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель на погоне Императора Николая II не читается, крест на шапке Михаила не касается сплошной черты. Лицо Императора Николая II вытянуто по вертикали, ушная раковина находится на одной линии с очертанием затылка. Пунктирная линия на аверсе начинается с точки ею же и заканчивается. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/ded/dede48dbae147b8bce8cccc4410512b9.jpg" rel="gr060901"><img alt="21разб-ав.jpg" src="/upload/medialibrary/ded/dede48dbae147b8bce8cccc4410512b9.jpg" title="21разб-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/ea3/ea3f9da177ee6ecca12396c7525b0718.jpg" rel="gr060901"><img alt="21разб-ухо.jpg" src="/upload/medialibrary/ea3/ea3f9da177ee6ecca12396c7525b0718.jpg" title="21разб-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/762/76223f15406c08dbcf1c7eada08c7c4f.jpg" rel="gr060901"><img alt="21разб-рев.jpg" src="/upload/medialibrary/762/76223f15406c08dbcf1c7eada08c7c4f.jpg" title="21разб-рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>10,7 гр.</b> диаметр-<b>28 мм. <br>
 </b>
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp; Особый чекан. Вензель на погоне Императора Николая II отсутствует.<br>
	 сплошной линии нет. Пунктирная линия на аверсе начинается с точки и заканчивается пунктиром. У Михаила крест на шее.<br>
	 Портреты Императоров в фас. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/562/562fc12257095918d2e6fa3da483ae5b.jpg" rel="gr060903"><img alt="23DSC_5618-ав.jpg" src="/upload/medialibrary/562/562fc12257095918d2e6fa3da483ae5b.jpg" title="23DSC_5618-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/7fe/7fee0da7928c66366fdca5b74eadcd91.jpg" rel="gr060903"><img alt="23DSC_5615-ухо.jpg" src="/upload/medialibrary/7fe/7fee0da7928c66366fdca5b74eadcd91.jpg" title="23DSC_5615-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/ab2/ab25ec4804b3e6f187307d384f65e7d8.jpg" rel="gr060903"><img alt="23DSC_5620-рев.jpg" src="/upload/medialibrary/ab2/ab25ec4804b3e6f187307d384f65e7d8.jpg" title="23DSC_5620-рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>11,1 гр.</b> диаметр-<b>28,7 мм. <br>
 </b>
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp;&nbsp; Вензель "E" на погоне Императора Николая II не четко выражен, крест на шапке Михаила не касается черты. Пунктирная линия на аверсе начинается с точки и заканчивается линией. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/93f/93f7f2f5169e2419b0f6dd1f8c6fc57f.jpg" rel="gr100901"><img alt="9-ав.jpg" src="/upload/medialibrary/93f/93f7f2f5169e2419b0f6dd1f8c6fc57f.jpg" title="9-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/910/910a633c01ea26b0a8d6206f81c9fd59.jpg" rel="gr100901"><img alt="9-ухо.jpg" src="/upload/medialibrary/910/910a633c01ea26b0a8d6206f81c9fd59.jpg" title="9-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/c68/c68aa533b6266bf1d0f6fd684073de05.jpg" rel="gr100901"><img alt="9-рев.jpg" src="/upload/medialibrary/c68/c68aa533b6266bf1d0f6fd684073de05.jpg" title="9-рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>9,2 гр.</b> диаметр-<b>27,9 мм.</b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Особый тип "частника". Изображение Михаила слева ,а Николая справа. Линии на аверсе отсутствуют. Шрифт на реверсе отличается от "госчекана". <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/323/3230a9f837d333561f6668176a05035b.jpg" rel="gr110901"><img alt="1-ав.jpg" src="/upload/medialibrary/323/3230a9f837d333561f6668176a05035b.jpg" title="1-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/103/1037db6173f43eb4663b86f8052be9d2.jpg" rel="gr110901"><img alt="1-ухо.jpg" src="/upload/medialibrary/103/1037db6173f43eb4663b86f8052be9d2.jpg" title="1-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/4da/4da5e96cad0f55b2fae0be548c9a0ad3.jpg" rel="gr110901"><img alt="1-рев.jpg" src="/upload/medialibrary/4da/4da5e96cad0f55b2fae0be548c9a0ad3.jpg" title="1-рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>11,8 гр</b>. диаметр-<b>28,3 мм.</b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель "А" на погоне Императора Николая II слабо выражен, крест на шапке Михаила выходит за сплошную черту. Пунктирная линия на аверсе начинается с пунктира им же и заканчивается. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/129/1296f5a382b29930e11b18ffb79c7e5a.jpg" rel="gr110902"><img alt="2-ав.jpg" src="/upload/medialibrary/129/1296f5a382b29930e11b18ffb79c7e5a.jpg" title="2-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/766/766f5ee252b0f6b572ae94f8ed05e316.jpg" rel="gr110902"><img alt="2-ухо.jpg" src="/upload/medialibrary/766/766f5ee252b0f6b572ae94f8ed05e316.jpg" title="2-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/574/574a84c785529f3d1d57cfd891edbb10.jpg" rel="gr110902"><img alt="2-рев.jpg" src="/upload/medialibrary/574/574a84c785529f3d1d57cfd891edbb10.jpg" title="2-рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>8,6 гр.</b> диаметр-<b>28,4 мм.</b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp;&nbsp; Вензель прописной буквой "А" на погоне Императора Николая II четко выражен, крест на шапке Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается с точки ею же и заканчивается. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/6fc/6fc429c32857f9fc4ba90e26d084ef94.jpg" rel="gr110903"><img alt="3-ав.jpg" src="/upload/medialibrary/6fc/6fc429c32857f9fc4ba90e26d084ef94.jpg" title="3-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/532/532fe32cce1d4ea78161221d31ff5446.jpg" rel="gr110903"><img alt="3-ухо.jpg" src="/upload/medialibrary/532/532fe32cce1d4ea78161221d31ff5446.jpg" title="3-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/97a/97a4977caa6f7758c3b15ac430312db8.jpg" rel="gr110903"><img alt="3-рев.jpg" src="/upload/medialibrary/97a/97a4977caa6f7758c3b15ac430312db8.jpg" title="3-рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>13,1 гр.</b> диаметр-<b>28,6 мм.</b>
</p>
<p style="text-align: center;">
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель "А" на погоне Императора Николая II не четко выражен, крест на шапке Михаила не касается сплошной черты.<br>
	 Пунктирная линия на аверсе начинается с точки и точкой заканчивается . <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/c62/c62db5bcb4e7b494f968a51d09c6670c.jpg" rel="gr120901"><img alt="7-ав.jpg" src="/upload/medialibrary/c62/c62db5bcb4e7b494f968a51d09c6670c.jpg" title="7-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/1ac/1ac772baf11f7b5b0aa88af06e030a1c.jpg" rel="gr120901"><img alt="7-ухо.jpg" src="/upload/medialibrary/1ac/1ac772baf11f7b5b0aa88af06e030a1c.jpg" title="7-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/3b7/3b7db558893a37eb678ea08cb1354729.jpg" rel="gr120901"><img alt="7-рев.jpg" src="/upload/medialibrary/3b7/3b7db558893a37eb678ea08cb1354729.jpg" title="7-рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>10,3 гр.</b> диаметр-<b>28,2 мм.</b>
</p>
<div>
	 Вензель печатной буквой "А" на погоне Императора Николая II не четко выражен, крест на шапке Михаила выходит за сплошную черту.Пунктирная линия на аверсе начинается с точки и заканчивается пунктиром. <b><br>
 </b>
	<hr>
</div>
<p style="text-align: center;">
 <a href="/upload/medialibrary/ad8/ad8b953b1a9483d51f10e6536b854be1.jpg" rel="gr130901"><img alt="12-аверс.jpg" src="/upload/medialibrary/ad8/ad8b953b1a9483d51f10e6536b854be1.jpg" title="12-аверс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/ff5/ff514ced566db7825f12b83bc9e03816.jpg" rel="gr130901"><img alt="12-ухо.jpg" src="/upload/medialibrary/ff5/ff514ced566db7825f12b83bc9e03816.jpg" title="12-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/17d/17d99ca2c8d2de3c68bf11ab8decaabe.jpg" rel="gr130901"><img alt="12-рев.jpg" src="/upload/medialibrary/17d/17d99ca2c8d2de3c68bf11ab8decaabe.jpg" title="12-рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>11,8 гр.</b> диаметр-<b>28,5 мм.</b>
</p>
<p style="text-align: left;">
</p>

	 &nbsp;&nbsp;&nbsp; Вензель "E" прописной буквой на погоне Императора Николая II четко выражен, крест на шапке Михаила не касается черты.Пунктирная линия на аверсе начинается с точки и переходит в цепь на шее Михаила Романова.<p> 
</p>
<hr><p style="text-align: center;">
 <a href="/upload/medialibrary/36e/36ea7742060e5cc9790f30afce19ea5e.jpg" rel="gr13092"><img alt="17-аверс.jpg" src="/upload/medialibrary/36e/36ea7742060e5cc9790f30afce19ea5e.jpg" title="17-аверс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/6c8/6c8ec050ffda1684ec292e099c41e785.jpg" rel="gr13092"><img alt="17-ухо.jpg" src="/upload/medialibrary/6c8/6c8ec050ffda1684ec292e099c41e785.jpg" title="17-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/f5e/f5ebb56e28ae714ec11c795e05f91403.jpg" rel="gr13092"><img alt="17-реверс.jpg" src="/upload/medialibrary/f5e/f5ebb56e28ae714ec11c795e05f91403.jpg" title="17-реверс.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>13,9 гр.</b> диаметр-<b>28,3 мм. <br>
 </b>
</p>
<p style="text-align: left;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель на погоне Императора Николая II не читается, крест на шапке Михаила касается черты. Пунктирная линия на аверсе начинается с линии ей же и заканчивается. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/85a/85a2027053eac91fb2274ae98ed50727.jpg" rel="gr130903"><img alt="16-а.jpg" src="/upload/medialibrary/85a/85a2027053eac91fb2274ae98ed50727.jpg" title="16-а.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/9f8/9f8c39a55e63a2c2609f85ae9fb9d03f.jpg" rel="gr130903"><img alt="16-ухо.jpg" src="/upload/medialibrary/9f8/9f8c39a55e63a2c2609f85ae9fb9d03f.jpg" title="16-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/180/18033a629a80fcf88b0b5b228d5a1bc2.jpg" rel="gr130903"><img alt="16-р.jpg" src="/upload/medialibrary/180/18033a629a80fcf88b0b5b228d5a1bc2.jpg" title="16-р.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>8,9 гр.</b> диаметр-<b>28,2 мм. <br>
 </b>
</p>
<p style="text-align: left;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель "А" на погоне Императора Николая II не четко выражен, крест на шапке Михаила касается черты. Орнамент шапки сложный. Пунктирная линия на аверсе начинается с точки , ею и заканчивается. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/163/16302e9f50e5cefd914ac8c22f10956a.jpg" rel="gr170901"><img alt="14-ав.jpg" src="/upload/medialibrary/163/16302e9f50e5cefd914ac8c22f10956a.jpg" title="14-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/299/2999209294d8d261af6fa0416ee5f4d1.jpg" rel="gr170901"><img alt="14-ухо.jpg" src="/upload/medialibrary/299/2999209294d8d261af6fa0416ee5f4d1.jpg" title="14-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/d80/d807b8bc98720575a4239359e936f0d2.jpg" rel="gr170901"><img alt="14-рев.jpg" src="/upload/medialibrary/d80/d807b8bc98720575a4239359e936f0d2.jpg" title="14-рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>11,9 гр.</b> диаметр-<b>28,5 мм.</b>
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp;&nbsp; Вензель "E" прописной буквой на погоне Императора Николая II четко выражен, крест на шапке Михаила не касается черты. Пунктирная линия на аверсе начинается с точки и заканчивается пунктиром. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/cac/cace2a5b9594125a2f38877c6cf2163b.jpg" rel="gr190902"><img alt="5 аверс-н.jpg" src="/upload/medialibrary/cac/cace2a5b9594125a2f38877c6cf2163b.jpg" title="5 аверс-н.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/f8e/f8ec0e2a2ae7854b747c4abcb90bae3b.jpg" rel="gr190902"><img alt="5-ухо y.jpg" src="/upload/medialibrary/f8e/f8ec0e2a2ae7854b747c4abcb90bae3b.jpg" title="5-ухо y.jpg" width="175" height="163" align="middle"></a><a href="/upload/medialibrary/567/567c56ff77954956f8d7eb1ba2bf1d45.jpg" rel="gr190902"><img alt="5-реверс-н.jpg" src="/upload/medialibrary/567/567c56ff77954956f8d7eb1ba2bf1d45.jpg" title="5-реверс-н.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>13,8 гр.</b> диаметр-<b>28,5 мм.</b>
</p>
<p style="text-align: left;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель "А" на погоне Императора Николая II не четко выражен, крест на шапке Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается с точки и заканчивается пунктирной линией. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/092/092939d349fe22d74e6c336c9537af28.jpg" rel="gr170903"><img alt="25-аверс.jpg" src="/upload/medialibrary/092/092939d349fe22d74e6c336c9537af28.jpg" title="25-аверс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/205/205fe202c94393ac36b2b107243a0f5d.jpg" rel="gr170903"><img alt="25-ухо.jpg" src="/upload/medialibrary/205/205fe202c94393ac36b2b107243a0f5d.jpg" title="25-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/d3b/d3bf66f2a6480f14db0a2facf68a7b92.jpg" rel="gr170903"><img alt="25-реверс.jpg" src="/upload/medialibrary/d3b/d3bf66f2a6480f14db0a2facf68a7b92.jpg" title="25-реверс.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>10,1 гр.</b> диаметр-<b>27,6 мм.</b>
</p>
<p style="text-align: center;">
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель на погоне Императора Николая II отсутствует, крест на шапке Михаила касается сплошной черты. Пунктирная линия на аверсе начинается с пунктира и заканчивается точкой. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/cac/cace2a5b9594125a2f38877c6cf2163b.jpg" rel="gr190901"><img alt="5 аверс-н.jpg" src="/upload/medialibrary/cac/cace2a5b9594125a2f38877c6cf2163b.jpg" title="5 аверс-н.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/f8e/f8ec0e2a2ae7854b747c4abcb90bae3b.jpg" rel="gr190901"><img alt="5-ухо y.jpg" src="/upload/medialibrary/f8e/f8ec0e2a2ae7854b747c4abcb90bae3b.jpg" title="5-ухо y.jpg" width="176" height="164" align="middle"></a><a href="/upload/medialibrary/567/567c56ff77954956f8d7eb1ba2bf1d45.jpg" rel="gr190901"><img alt="5-реверс-н.jpg" src="/upload/medialibrary/567/567c56ff77954956f8d7eb1ba2bf1d45.jpg" title="5-реверс-н.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>13,8 гр.</b> диаметр-<b>28,5 мм.</b>
</p>
<p style="text-align: center;">
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp;&nbsp; Вензель "А" на погоне Императора Николая II не четко выражен, крест на шапке Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается с точки и заканчивается пунктирной линией. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/371/37104719e9f842ab98451261c09f32d5.jpg" rel="gr190902"><img alt="24-ав.jpg" src="/upload/medialibrary/371/37104719e9f842ab98451261c09f32d5.jpg" title="24-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/f04/f04126831f05a327bd2d3d6ffed0a7f3.jpg" rel="gr190902"><img alt="24-ухо.jpg" src="/upload/medialibrary/f04/f04126831f05a327bd2d3d6ffed0a7f3.jpg" title="24-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/adc/adc7d0c3a27e8632341c42d9bce4b7d3.jpg" rel="gr190902"><img alt="24-рев.jpg" src="/upload/medialibrary/adc/adc7d0c3a27e8632341c42d9bce4b7d3.jpg" title="24-рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>9,6 гр.</b> диаметр-<b>27,9 мм.</b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель на погоне Императора Николая II не четко выражен, отсутствует сплошная черта. Пунктирная линия на аверсе начинается с пунктира им же и заканчивается. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/518/518d0aab8d1b38790bed0b5abe198fec.jpg" rel="gr100903"><img alt="22 DSC_5945-аверс.jpg" src="/upload/medialibrary/518/518d0aab8d1b38790bed0b5abe198fec.jpg" title="22 DSC_5945-аверс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/2c0/2c07d6831467541e7aff8e56b750d801.jpg" rel="gr100903"><img alt="22 DSC_5943-ухо.jpg" src="/upload/medialibrary/2c0/2c07d6831467541e7aff8e56b750d801.jpg" title="22 DSC_5943-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/f8f/f8fcf21905919ece88a15fd8c7caa990.jpg" rel="gr100903"><img alt="22 DSC_5949-рев.jpg" src="/upload/medialibrary/f8f/f8fcf21905919ece88a15fd8c7caa990.jpg" title="22 DSC_5949-рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>серебро 14,3 гр.</b> диаметр-<b>28,7 мм.</b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; На ухе клеймо производителя "ЛИ".Вензель "S" на погоне Императора Николая II не четко выражен, крест на шапке Михаила касается сплошной черты.Пунктирная линия на аверсе начинается пунктирной линией ей и заканчивается. <br>
</p>
<p>
 <a href="/upload/medialibrary/18e/18ee0af0dc126cecc6af7653ac4c10f9.jpg"></a>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/18e/18ee0af0dc126cecc6af7653ac4c10f9.jpg" rel="gr27.12-1"><img alt="плоск.рельеф-серебро-аверс.jpg" src="/upload/medialibrary/18e/18ee0af0dc126cecc6af7653ac4c10f9.jpg" title="плоск.рельеф-серебро-аверс.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/c98/c9834971276b5d12a80aa368e218ac98.jpg" rel="gr27.12-1"><img alt="плоск.рельеф-серебро-ухо.jpg" src="/upload/medialibrary/c98/c9834971276b5d12a80aa368e218ac98.jpg" title="плоск.рельеф-серебро-ухо.jpg" width="170" height="177" align="middle"></a><a href="/upload/medialibrary/36c/36c7246bbaaecdf091031aa3344e66ba.jpg" rel="gr27.12-1"><img alt="плоск.рельеф-серебро-реверс.jpg" src="/upload/medialibrary/36c/36c7246bbaaecdf091031aa3344e66ba.jpg" title="плоск.рельеф-серебро-реверс.jpg" width="180" height="180" align="middle"></a>
</p>
<p>
 <a href="/upload/medialibrary/36c/36c7246bbaaecdf091031aa3344e66ba.jpg"></a>
</p>
<p>
</p>
<p>
</p>
<p style="text-align: center;">
	 Вес-<b>10,5 гр.</b> диаметр-<b>27,6 мм.</b>
</p>
<p>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p style="text-align: center;">
	 &nbsp; Клеймо производителя "ФГ", проба "84" соответствует пробе после 1908 г. Вензель на погоне Императора Николая II "S", крест на шапке Михаила касается сплошной черты.Пунктирная линия на аверсе начинается с пунктира и заканчивается точкой. <br></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/ee6/ee6e76ca0216753c98345e8ba00d4655.jpg" rel="Up-18-06-2020-2"><img alt="ДР-лот-387-МиМ-ав.jpg" src="/upload/medialibrary/ee6/ee6e76ca0216753c98345e8ba00d4655.jpg" title="ДР-лот-387-МиМ-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/ac4/ac46a0cc897894e49148e85032e7e837.jpg" rel="Up-18-06-2020-2"><img alt="ДР-лот-387-МиМ-ухо.jpg" src="/upload/medialibrary/ac4/ac46a0cc897894e49148e85032e7e837.jpg" title="ДР-лот-387-МиМ-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/5cb/5cb21fa4157e8c4966ff3f4ef9892cb1.jpg" rel="Up-18-06-2020-2"><img alt="ДР-лот-387-МиМ-рв.jpg" src="/upload/medialibrary/5cb/5cb21fa4157e8c4966ff3f4ef9892cb1.jpg" title="ДР-лот-387-МиМ-рв.jpg" width="185" height="185" align="middle"></a></p><p>
	 </p><p style="text-align: center;">Вес-<b>13,6 гр</b>. диаметр-<b>28,4 мм.</b></p><p></p><div>

	 &nbsp;&nbsp; Вензель на погоне Императора Николая II "А" , крест на шапке Михаила&nbsp; касается сплошной черты. Пунктирная линия на аверсе начинается с точки и точкой же и заканчивается. Портретное сходство Императоров приблизительное.</div><br>
<div><hr>
</div><p style="text-align: center;"><a href="/upload/medialibrary/463/4639c189d32d1786197e2a551d4ecb95.jpg" rel="18-06-2020-2"><img alt="ДР-лот-388-МиМ-ав.jpg" src="/upload/medialibrary/463/4639c189d32d1786197e2a551d4ecb95.jpg" title="ДР-лот-388-МиМ-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/bcc/bccae84ff5ee025e5273826125d2536d.jpg" rel="18-06-2020-2"><img alt="ДР-лот-388-МиМ-ухо.jpg" src="/upload/medialibrary/bcc/bccae84ff5ee025e5273826125d2536d.jpg" title="ДР-лот-388-МиМ-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/646/64635a69aa9da2aa517fa80882aa5ec9.jpg" rel="18-06-2020-2"><img alt="ДР-лот-388-МиМ-рв.jpg" src="/upload/medialibrary/646/64635a69aa9da2aa517fa80882aa5ec9.jpg" title="ДР-лот-388-МиМ-рв.jpg" width="185" height="185" align="middle"></a></p><p></p><p style="text-align: center;">Вес-<b>13,9 гр</b>. диаметр-<b>28,9 мм.</b></p><p></p><p></p><div>

	 &nbsp;&nbsp; Вензель на погоне Императора Николая II "А" слабо выражен , крест на шапке 
Михаила&nbsp; касается сплошной черты. Пунктирная линия на аверсе начинается с штриха им же и заканчивается. Портретное сходство Императоров 
приблизительное, у Николая II монголоидный тип лица.</div>
<hr><p style="text-align: center;"><a href="/upload/medialibrary/936/936a13e7bee7674b65561ebb2bde795f.jpg" rel="31-07-2020-1"><img alt="300-лет-ДР-позолота-ср-рел-ав.jpg" src="/upload/medialibrary/936/936a13e7bee7674b65561ebb2bde795f.jpg" title="300-лет-ДР-позолота-ср-рел-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/703/703251afe44ff5b1f991aa8252d9b61b.jpg" rel="31-07-2020-1"><img alt="300-лет-ДР-позолота-ср-рел-ухо.jpg" src="/upload/medialibrary/703/703251afe44ff5b1f991aa8252d9b61b.jpg" title="300-лет-ДР-позолота-ср-рел-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/9cb/9cb61486c324926cc03a5e28bd731514.jpg" rel="31-07-2020-1"><img alt="300-лет-ДР-позолота-ср-рел-рв.jpg" src="/upload/medialibrary/9cb/9cb61486c324926cc03a5e28bd731514.jpg" title="300-лет-ДР-позолота-ср-рел-рв.jpg" width="185" height="185" align="middle"></a></p><p style="text-align: center;"> Вес-<b>11,6 гр</b>. диаметр-<b>28,5 мм. </b></p><p>
</p><p></p>
<p>

</p>

<p>&nbsp;  Вензель на погоне Императора Николая II "Е II" с погонной пуговицей , 
крест на шапке Михаила не касается сплошной черты. Пунктирная линия на 
аверсе начинается с пунктира им же и заканчивается.</p>
<hr><p style="text-align: center;"><a href="/upload/medialibrary/065/065e2130aa884c1e1b35145d58981163.jpg" rel="2-31-07-2020"><img alt="300-лет-ДР-низ-рельеф-медь-ав.jpg" src="/upload/medialibrary/065/065e2130aa884c1e1b35145d58981163.jpg" title="300-лет-ДР-низ-рельеф-медь-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/c90/c90b36320e6ec94015296f51e9b07759.jpg" rel="2-31-07-2020"><img alt="300-лет-ДР-низ-рельеф-медь-ухо.jpg" src="/upload/medialibrary/c90/c90b36320e6ec94015296f51e9b07759.jpg" title="300-лет-ДР-низ-рельеф-медь-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/a1a/a1a3c049be581f675e17dc8e64bf4f1b.jpg" rel="2-31-07-2020"><img alt="300-лет-ДР-низ-рельеф-медь-рв.jpg" src="/upload/medialibrary/a1a/a1a3c049be581f675e17dc8e64bf4f1b.jpg" title="300-лет-ДР-низ-рельеф-медь-рв.jpg" width="185" height="185" align="middle"></a></p><p> </p><p style="text-align: center;">Вес-<b>11,7 гр</b>. диаметр-<b>28,5 мм. </b></p><p></p><p></p><p>
</p>
<p>


</p>

<p>&nbsp;  Вензель на погоне Императора Николая II стилизован под "Е ", 
крест на шапке Михаила не касается сплошной черты. Пунктирная линия на 
аверсе начинается с точки и заканчивается пунктиром. Лицо Императора Николая II узкое, глазницы на портретах выделяются.</p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/c86/c86673917cf41f54e4a7e9e65903df03.jpg" rel="12-11-2020"><img alt="300-лет-дР-ав.jpg" src="/upload/medialibrary/c86/c86673917cf41f54e4a7e9e65903df03.jpg" title="300-лет-дР-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/db3/db39b263cdc25d1a869f8f1f64b219c1.jpg" rel="12-11-2020"><img alt="300-лет-дР-ухо.jpg" src="/upload/medialibrary/db3/db39b263cdc25d1a869f8f1f64b219c1.jpg" title="300-лет-дР-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/c72/c72af7a02d9a0fa83c5217aceaacf1b6.jpg" rel="12-11-2020"><img alt="300-лет-дР-рв.jpg" src="/upload/medialibrary/c72/c72af7a02d9a0fa83c5217aceaacf1b6.jpg" title="300-лет-дР-рв.jpg" width="185" height="185" align="middle"></a></p><p></p><p style="text-align: center;">Вес-<b>12,3 гр</b>. диаметр-<b>28,7 мм. </b></p><p></p><p></p><p>
</p>
<p>


</p><div>

&nbsp;  Вензель на погоне Императора Николая II&nbsp; "А ", 
крест на шапке Михаила не касается сплошной черты. Пунктирная линия на 
аверсе начинается с точки и заканчивается пунктиром. <br>
<hr>
</div><p style="text-align: center;"><a href="/upload/medialibrary/54a/54a556b3c2cb74a0a0c8faae6115d111.jpg" rel="NY-29-12-2020"><img alt="юв-ав.jpg" src="/upload/medialibrary/54a/54a556b3c2cb74a0a0c8faae6115d111.jpg" title="юв-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/a04/a040b57e6f12cd9cb4277746f1187419.jpg" rel="NY-29-12-2020"><img alt="юв-ухо.jpg" src="/upload/medialibrary/a04/a040b57e6f12cd9cb4277746f1187419.jpg" title="юв-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/fac/faceb30a1e4e804ef3762c43f66a5a7e.jpg" rel="NY-29-12-2020"><img alt="юв-рв.jpg" src="/upload/medialibrary/fac/faceb30a1e4e804ef3762c43f66a5a7e.jpg" title="юв-рв.jpg" width="185" height="185" align="middle"></a></p><p style="text-align: center;">Вес-<b>13,1 гр.</b> диаметр-<b>28 мм. </b><br>Вензель "А" на погоне Императора Николая II. Крест на шапке Михаила 
касается сплошной черты. Пунктирная линия на аверсе начинается с 
пунктира им же и заканчивается. На медали отсутствует борт.Гурт 
полирован. Вариант с глубокой позолотой. Скорее всего производство 
ювелирной мастерской по персональному заказу. <br></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/b8a/b8af8b4cbee6426f59f105f2bd55ee63.jpg" rel="g-1-10-03-2021"><img alt="300-лет-ДР-МЛК-ср.рел.ав.jpg" src="/upload/medialibrary/b8a/b8af8b4cbee6426f59f105f2bd55ee63.jpg" title="300-лет-ДР-МЛК-ср.рел.ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/5e7/5e759a5910cd5c6182d8eeda564b65a6.jpg" rel="g-1-10-03-2021"><img alt="300-лет-ДР-МЛК-ср.рел.ухо.jpg" src="/upload/medialibrary/5e7/5e759a5910cd5c6182d8eeda564b65a6.jpg" title="300-лет-ДР-МЛК-ср.рел.ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/b35/b35109b26ad0463e4d3d6579cd0f864d.jpg" rel="g-1-10-03-2021"><img alt="300-лет-ДР-МЛК-ср.рел.рв.jpg" src="/upload/medialibrary/b35/b35109b26ad0463e4d3d6579cd0f864d.jpg" title="300-лет-ДР-МЛК-ср.рел.рв.jpg" width="185" height="185" align="middle"></a></p><p style="text-align: center;">Вес-<b>13,6 гр.</b> диаметр-<b>28,6 мм.</b> <br>
    Вензель "А" на погоне Императора Николая II не четко выражен, крест 
на шапке Михаила не касается сплошной черты. Глазницы Императоров чётко 
выражены. <br>
Пунктирная линия на аверсе начинается с точки и заканчивается пунктиром. <br></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/f1d/f1d9f91722488057af86154f419f7302.jpg" rel="01-06-2021-1"><img alt="300-лет-ДР-ав.jpg" src="/upload/medialibrary/f1d/f1d9f91722488057af86154f419f7302.jpg" title="300-лет-ДР-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/230/2303e77981e80c0f9899b2ada3384d0c.jpg" rel="01-06-2021-1"><img alt="300-лет-ДР-ухо.jpg" src="/upload/medialibrary/230/2303e77981e80c0f9899b2ada3384d0c.jpg" title="300-лет-ДР-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/823/8235d996d9abaa92772271a835f7472c.jpg" rel="01-06-2021-1"><img alt="300-лет-ДР-рв.jpg" src="/upload/medialibrary/823/8235d996d9abaa92772271a835f7472c.jpg" title="300-лет-ДР-рв.jpg" width="185" height="185" align="middle"></a></p><p style="text-align: center;">Вес-<b>12,6 гр.</b> диаметр-<b>28,36 мм.</b><br>
Вензель "А" на погоне Императора Николая II не четко выражен, крест на 
шапке Михаила касается сплошной черты и немного заходит за неё. Глазницы
 Императоров чётко выражены. Пунктирная линия на аверсе начинается с пунктира им же и заканчивается. <br></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/3e7/3e75f21ed046d020b35c9083928a6aed.jpg" rel="16-08-2021-2"><img alt="300-лет-ДР-ав.jpg" src="/upload/medialibrary/3e7/3e75f21ed046d020b35c9083928a6aed.jpg" title="300-лет-ДР-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/842/842f01a91e5b0923ad2cc28db9013f93.jpg" rel="16-08-2021-2"><img alt="300-лет-ДР-ухо.jpg" src="/upload/medialibrary/842/842f01a91e5b0923ad2cc28db9013f93.jpg" title="300-лет-ДР-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/cb1/cb17e9798515bf47417914e1ae821c4a.jpg" rel="16-08-2021-2"><img alt="300-лет-ДР-рв.jpg" src="/upload/medialibrary/cb1/cb17e9798515bf47417914e1ae821c4a.jpg" title="300-лет-ДР-рв.jpg" width="185" height="185" align="middle"></a></p><blockquote><p style="text-align: center;"></p></blockquote><p style="text-align: center;"></p><p style="text-align: center;">Вес-<b>10,7 гр. </b>диаметр-<b>27,55 мм.</b><br>Позолота. Вензель на погоне Императора Николая II не читается, крест на шапке Михаила  касается сплошной черты. Пунктирная линия на аверсе начинается с пунктира и заканчивается точкой. Шрифт на реверсе тонкий.</p>

<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/f8b/f8bdb2d5e9b0fcaa6b626bed5fd7b9e8.jpg" rel="18.08.2021-1"><img alt="300-лет-ДР-Е-ав.jpg" src="/upload/medialibrary/f8b/f8bdb2d5e9b0fcaa6b626bed5fd7b9e8.jpg" title="300-лет-ДР-Е-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/d22/d2271e797a09084a79233242fd4dbd90.jpg" rel="18.08.2021-1"><img alt="300-лет-ДР-Е-ухо.jpg" src="/upload/medialibrary/d22/d2271e797a09084a79233242fd4dbd90.jpg" title="300-лет-ДР-Е-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/b59/b59f4847d25bb2f886a401b0245cdd4b.jpg" rel="18.08.2021-1"><img alt="300-лет-ДР-Е-рв.jpg" src="/upload/medialibrary/b59/b59f4847d25bb2f886a401b0245cdd4b.jpg" title="300-лет-ДР-Е-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;"></p>
<p style="text-align: center;">Вес-<b>12,1 гр. </b>диаметр-<b>28,32 мм.</b><br>Вензель "Е" на погоне Императора Николая II&nbsp; четко выражен, крест на 
шапке Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается с точки ею же и заканчивается. <br></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/41c/41c403f0aee0bb90c0eedb37c46e0a54.jpg" rel="18.08.2021-2"><img alt="300-лет-ДР-гру-ав.jpg" src="/upload/medialibrary/41c/41c403f0aee0bb90c0eedb37c46e0a54.jpg" title="300-лет-ДР-гру-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/105/105a53a017e4404f470933b76afbbf8c.jpg" rel="18.08.2021-2"><img alt="300-лет-ДР-гру-ухо.jpg" src="/upload/medialibrary/105/105a53a017e4404f470933b76afbbf8c.jpg" title="300-лет-ДР-гру-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/293/293a8de07c12dd9eeac5b6f801e4fba5.jpg" rel="18.08.2021-2"><img alt="300-лет-ДР-гру-рв.jpg" src="/upload/medialibrary/293/293a8de07c12dd9eeac5b6f801e4fba5.jpg" title="300-лет-ДР-гру-рв.jpg" width="185" height="185" align="middle"></a></p>
<p style="text-align: center;"></p>
<p style="text-align: center;">Вес-<b>13,1 гр. </b>диаметр-<b>28,71 мм.</b><br>Вензель "А" на погоне Императора Николая II.Крест на шапке Михаила не 
касается сплошной черты.Пунктирная линия на аверсе начинается с точки и 
заканчивается пунктиром. На обрезе медали инициалы мастерской "ГРУ".<br></p>
<hr>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>