<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Плоский рельеф");
?><p style="text-align: center;">
 <b><span style="font-size: 15pt;">Плоский рельеф.</span></b>
</p>
<div style="background-color: white">
<p style="text-align: center;">
 <b><span style="font-size: 15pt;"><a href="/upload/medialibrary/684/684369b4594e6cb567613028d3d8ccf1.jpg" rel="group0"><img alt="1-аверс-м.jpg" src="/upload/medialibrary/684/684369b4594e6cb567613028d3d8ccf1.jpg" title="1-аверс-м.jpg" width="171" height="205" align="middle"></a><a href="/upload/medialibrary/1d4/1d4b529215a03e808886344061d46e1b.jpg" rel="group0"><img alt="1-ухо-м.jpg" src="/upload/medialibrary/1d4/1d4b529215a03e808886344061d46e1b.jpg" title="1-ухо-м.jpg" width="183" height="171" align="middle"></a><a href="/upload/medialibrary/b04/b04232bbad608dacf4f86931e16e9b01.jpg" rel="group0"><img alt="1-реверс-м.jpg" src="/upload/medialibrary/b04/b04232bbad608dacf4f86931e16e9b01.jpg" title="1-реверс-м.jpg" width="171" height="205" align="middle"></a><br>
 </span></b>
</p>
<p style="text-align: center;">
 <br>
	 Вес-<b>11,6 гр.</b> диаметр-<b>28,4 мм.</b>
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp;&nbsp; Вензель на погоне Императора Николая II не читается, крест на шапке Михаила не касается сплошной черты.Усы Императора Николая II сильно закручены вверх. Пунктирная линия на аверсе начинается с точки и точкой же заканчивается.
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/489/489a596f904fafe8669d4184d9e62b9a.jpg" rel="group01"><img alt="2-аверс-2-м.jpg" src="/upload/medialibrary/489/489a596f904fafe8669d4184d9e62b9a.jpg" title="2-аверс-2-м.jpg" width="170" height="205" align="middle"></a><a href="/upload/medialibrary/fd8/fd829f3e3def9e12b68664e7494d6683.jpg" rel="group01"><img alt="2-ухо.jpg" src="/upload/medialibrary/fd8/fd829f3e3def9e12b68664e7494d6683.jpg" title="2-ухо.jpg" width="171" height="153" align="middle"></a><a href="/upload/medialibrary/f25/f25a333d20068087c5a33d5d124c118f.jpg" rel="group01"><img alt="2-реверс-м.jpg" src="/upload/medialibrary/f25/f25a333d20068087c5a33d5d124c118f.jpg" title="2-реверс-м.jpg" width="171" height="208" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>9,7 гр</b>. диаметр-<b>28,3 мм.</b><br>
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp; Вензель на погоне Императора Николая II "А" выглядит как "Л", крест на шапке Михаила не касается сплошной черты.<br>
	 Пунктирная линия на аверсе начинается с точки и точкой же заканчивается. <br>
</p>
<p style="text-align: left;">
</p>
<hr>
<p>
</p>
<p style="text-align: center;">
 <a href="/upload/medialibrary/cce/cceab0e8562270a02a1c3517d79ae63b.jpg" rel="group001"><img alt="4-аверс.jpg" src="/upload/medialibrary/cce/cceab0e8562270a02a1c3517d79ae63b.jpg" title="4-аверс.jpg" width="178" height="205" align="middle"></a><a href="/upload/medialibrary/916/916ad66ea855d345ba9e0085629b9b14.jpg" rel="group001"><img alt="4-ухо.jpg" src="/upload/medialibrary/916/916ad66ea855d345ba9e0085629b9b14.jpg" title="4-ухо.jpg" width="171" height="140" align="middle"></a><a href="/upload/medialibrary/d5e/d5e0ba6d5680ddcaa8956d1c2d5bbe7f.jpg" rel="group001"><img alt="4-реверс.jpg" src="/upload/medialibrary/d5e/d5e0ba6d5680ddcaa8956d1c2d5bbe7f.jpg" title="4-реверс.jpg" width="175" height="205" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>13,7 гр</b>. диаметр-<b>28,4 мм. </b>
</p>
<p style="text-align: center;">
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp; Вензель на погоне Императора Николая II неразборчив ,усы слабо закруглены вверх. Крест на шапке Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается со штриха и заканчивается точкой.
</p>
<p style="text-align: center;">
</p>
<p>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/2f5/2f5e980538a952189e253a31f07cceb4.jpg" rel="group0001"><img alt="6-аверс-м.jpg" src="/upload/medialibrary/2f5/2f5e980538a952189e253a31f07cceb4.jpg" title="6-аверс-м.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/aae/aae3bae0f111f34105c303708698ee66.jpg" rel="group0001"><img alt="6-ухо.jpg" src="/upload/medialibrary/aae/aae3bae0f111f34105c303708698ee66.jpg" title="6-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/863/863a6a497eebd3510433003ff4285415.jpg" rel="group0001"><img alt="6-реверс-м.jpg" src="/upload/medialibrary/863/863a6a497eebd3510433003ff4285415.jpg" title="6-реверс-м.jpg" width="175" height="175" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>11,4 гр.</b> диаметр-<b>28,7 мм. </b>
</p>
<p style="text-align: center;">
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp; Вензеля на погоне Императора Николая II нет. Крест на шапке Михаила не касается сплошной черты.<br>
	 Пунктирная линия на аверсе начинается со штриха и заканчивается точкой. Шрифт на реверсе больше.
</p>
<p style="text-align: center;">
</p>
<p>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/174/1744090bc40f533b36a075c37f73c49b.jpg" rel="group0000"><img alt="полк.галун-ав.jpg" src="/upload/medialibrary/174/1744090bc40f533b36a075c37f73c49b.jpg" title="полк.галун-ав.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/cd3/cd3d602107d45cd9f1c70665e7c84408.jpg" rel="group0000"><img alt="полк.галун-ухо.jpg" src="/upload/medialibrary/cd3/cd3d602107d45cd9f1c70665e7c84408.jpg" title="полк.галун-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/0b4/0b4dadff659f1b6d1d5da85ac8c1b29a.jpg" rel="group0000"><img alt="полк.галун-рев.jpg" src="/upload/medialibrary/0b4/0b4dadff659f1b6d1d5da85ac8c1b29a.jpg" title="полк.галун-рев.jpg" width="175" height="175" align="middle"></a>
</p>
<p style="text-align: center;">
</p>
<p align="center">
	 Вес-<b>9,12 гр.</b> диаметр-<b>28,3 мм.</b>
</p>
<p style="text-align: left;" align="center">
	 &nbsp;&nbsp;&nbsp; Вензель на погоне Императора Николая II "А" не четкий, погон с галуном полковника. Крест на шапке Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается с точки и точкой же заканчивается. <br>
</p>
<hr>
<p align="center">
 <a href="/upload/medialibrary/4b9/4b9aad893469949b901a912a8c8e9d99.jpg" rel="gr1-21"><img alt="плоский-рельеф-8-аверс.jpg" src="/upload/medialibrary/4b9/4b9aad893469949b901a912a8c8e9d99.jpg" title="плоский-рельеф-8-аверс.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/afb/afba519dcf1f56f4d8c2fbb847c48c68.jpg" rel="gr1-21"><img alt="плоский-рельеф-8-ухо.jpg" src="/upload/medialibrary/afb/afba519dcf1f56f4d8c2fbb847c48c68.jpg" title="плоский-рельеф-8-ухо.jpg" width="170" height="170" align="middle"></a><a href="/upload/medialibrary/0ad/0ad0931dd4f6ca834c79152e4b4321d1.jpg" rel="gr1-21"><img alt="плоский-рельеф-8-реверс.jpg" src="/upload/medialibrary/0ad/0ad0931dd4f6ca834c79152e4b4321d1.jpg" title="плоский-рельеф-8-реверс.jpg" width="175" height="175" align="middle"></a>
</p>
<p align="center">
	 Вес-<b>9,61 гр.</b> диаметр-<b>28,3 мм.</b>
</p>
<p align="center">
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель "А" на погоне Императора Николая II прописан как "Л", крест на шапке Михаила касается сплошной черты. Пунктирная линия на аверсе начинается с точки и точкой же заканчивается. На реверсе шрифт цифр другой. Сама надпись имеет небольшой сдвиг влево. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/bca/bcab80297530cfcc36acb3e5385c419c.jpg" rel="gr21-2"><img alt="9-новый-аверс-м.jpg" src="/upload/medialibrary/bca/bcab80297530cfcc36acb3e5385c419c.jpg" title="9-новый-аверс-м.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/404/4047f4f3d7fdce62636dc9660450e911.jpg" rel="gr21-2"><img alt="9-новый-аверс-про.jpg" src="/upload/medialibrary/404/4047f4f3d7fdce62636dc9660450e911.jpg" title="9-новый-аверс-про.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/129/129ffd07ceb0d4032c92e98c17eb50d3.jpg" rel="gr21-2"><img alt="новый-реверс.jpg" src="/upload/medialibrary/129/129ffd07ceb0d4032c92e98c17eb50d3.jpg" title="новый-реверс.jpg" width="175" height="175" align="middle"></a><br>
</p>
<p style="text-align: center;">
</p>
<p>
</p>
<p style="text-align: center;">
	 Вес-<b>9,4 гр.</b> диаметр-<b>28,8 мм.</b>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp; Вензель на погоне Императора Николая II просматривается буквой "Л". Отсутствие сплошной черты. Изображения Императоров в полуфас. Пунктирная линия на аверсе начинается с точки, ею и заканчивается . <br>
</p>
<p style="text-align: left;">
</p>
<hr>
<p>
</p>
<p style="text-align: center;">
 <a href="/upload/medialibrary/340/340214463caba7327410b143976572f0.jpg" rel="gr21-3"><img alt="9-ав.jpg" src="/upload/medialibrary/340/340214463caba7327410b143976572f0.jpg" title="9-ав.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/f85/f85d91e807864947a80d8a758a8a65f4.jpg" rel="gr21-3"><img alt="9-ухо.jpg" src="/upload/medialibrary/f85/f85d91e807864947a80d8a758a8a65f4.jpg" title="9-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/4a1/4a14d81628b67f4c9d66c31aa7f789c8.jpg" rel="gr21-3"><img alt="9-рев.jpg" src="/upload/medialibrary/4a1/4a14d81628b67f4c9d66c31aa7f789c8.jpg" title="9-рев.jpg" width="175" height="175" align="middle"></a>
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
	 Вес-<b>11,8 гр.</b> диаметр-<b>28,3 мм.</b>
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp;&nbsp; Аверс стандартный штамп. Крест на шапке Михаила касается сплошной черты. Пунктирная линия на аверсе начинается с точки , точкой и заканчивается. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/d30/d302f1d92ad124ed4d17847d025497fb.jpg" rel="gr21-3"><img alt="3-бк-ав.jpg" src="/upload/medialibrary/d30/d302f1d92ad124ed4d17847d025497fb.jpg" title="3-бк-ав.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/e50/e50e1677b8271a6488071450f6e95bd7.jpg" rel="gr21-3"><img alt="3-бк-ухо.jpg" src="/upload/medialibrary/e50/e50e1677b8271a6488071450f6e95bd7.jpg" title="3-бк-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/990/99096bf8f8791a3959df1ee11ff646b1.jpg" rel="gr21-3"><img alt="3-бк-рев.jpg" src="/upload/medialibrary/990/99096bf8f8791a3959df1ee11ff646b1.jpg" title="3-бк-рев.jpg" width="175" height="175" align="middle"></a>
</p>
<p style="text-align: center;">
	 Вес-<b>12,51 гр.</b> диаметр-<b>28,4 мм</b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель на погоне Императора Николая II "E" , крест на шапке Михаила не касается сплошной черты. Пунктирная линия на аверсе начинается с точки и заканчивается штрихом. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/c6e/c6ee9b97db9a1003da559e92f4977e35.jpg" rel="gr21-44"><img alt="5-ав.jpg" src="/upload/medialibrary/c6e/c6ee9b97db9a1003da559e92f4977e35.jpg" title="5-ав.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/90d/90d07b8b16abdbea80bf906ee9ce989c.jpg" rel="gr21-44"><img alt="5-ухо.jpg" src="/upload/medialibrary/90d/90d07b8b16abdbea80bf906ee9ce989c.jpg" title="5-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/3bd/3bde5f09c835b42877a1c34f91aa98ce.jpg" rel="gr21-44"><img alt="5-рев.jpg" src="/upload/medialibrary/3bd/3bde5f09c835b42877a1c34f91aa98ce.jpg" title="5-рев.jpg" width="175" height="175" align="middle"></a>
</p>
<p style="text-align: center;">
</p>
<p>
</p>
<p style="text-align: center;">
	 Вес-<b>9,4 гр.</b> диаметр-<b>28,8 мм.</b>
</p>
<p>
</p>
<p style="text-align: left;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензеля на погоне Императора Николая II нет,2 штриха посередине погона. Пунктирная линия на аверсе начинается со штриха ,штрихом и заканчиваются. Сплошной линии нет. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/4fd/4fddd3ead2df6afca28cb85f3c119f21.jpg" rel="gr-21-444"><img alt="7-плоский-рельеф-аверс-нов.jpg" src="/upload/medialibrary/4fd/4fddd3ead2df6afca28cb85f3c119f21.jpg" title="7-плоский-рельеф-аверс-нов.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/9aa/9aabbabd20fb12252bb3e9fb42cef195.jpg" rel="gr-21-444"><img alt="7-плоский-рельеф-ухо-yjd.jpg" src="/upload/medialibrary/9aa/9aabbabd20fb12252bb3e9fb42cef195.jpg" title="7-плоский-рельеф-ухо-yjd.jpg" width="170" height="170" align="middle"></a><a href="/upload/medialibrary/311/311db1825abd42e78cffa9e5be734832.jpg" rel="gr-21-444"><img alt="7-плоский-рельеф-реверс-нов.jpg" src="/upload/medialibrary/311/311db1825abd42e78cffa9e5be734832.jpg" title="7-плоский-рельеф-реверс-нов.jpg" width="175" height="175" align="middle"></a><br>
</p>
 <b> </b>
<p>
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
</p>
<p>
</p>
<p style="text-align: center;">
	 Вес-<b>12,0 гр.</b> диаметр-<b>28,4 мм.</b>
</p>
<p>
</p>
<p style="text-align: left;">
 <b> </b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Крест на шапке Михаила касается сплошной черты. Пунктирная линия на аверсе начинается с точки и заканчивается точкой. Штамп пескоструйный. <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/c3b/c3be3c1ffc4badf83114796b21044bbf.jpg" rel="gr27-12-2"><img alt="разб-аверс.jpg" src="/upload/medialibrary/c3b/c3be3c1ffc4badf83114796b21044bbf.jpg" title="разб-аверс.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/070/070f8d2517e17181afbd45bead06bbe8.jpg" rel="gr27-12-2"><img alt="разб-ухо.jpg" src="/upload/medialibrary/070/070f8d2517e17181afbd45bead06bbe8.jpg" title="разб-ухо.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/5a8/5a83ad4e162c8d496d5022e6b45aba2c.jpg" rel="gr27-12-2"><img alt="разб-реверс.jpg" src="/upload/medialibrary/5a8/5a83ad4e162c8d496d5022e6b45aba2c.jpg" title="разб-реверс.jpg" width="180" height="180" align="middle"></a>
</p>
<p style="text-align: center;">
</p>
<p align="center">
	 Вес-<b>11,5 гр.</b> диаметр-<b>28,5 мм.</b>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; Вензель на погоне Императора Николая II не читается, на шапке Михаила орнамент из точек, медальон не круглый а три черты. Отсутствует сплошная линия. Пунктирная линия на аверсе начинается с точки и точкой же заканчивается. <br>
</p>
<p>
</p>
<hr>
 <a href="/upload/medialibrary/6d8/6d872c60eb68321054fe32541a622c0d.jpg"></a>
<p>
</p>
<p style="text-align: center;">
 <a href="/upload/medialibrary/6d8/6d872c60eb68321054fe32541a622c0d.jpg" rel="r-11-031"><img alt="пл-рел-без-спл-ч-ав.jpg" src="/upload/medialibrary/6d8/6d872c60eb68321054fe32541a622c0d.jpg" title="пл-рел-без-спл-ч-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/f23/f23fd3626a92f07240c43ad67c99dadb.jpg" rel="r-11-031"><img alt="пл-рел-без-спл-ч-ухо.jpg" src="/upload/medialibrary/f23/f23fd3626a92f07240c43ad67c99dadb.jpg" title="пл-рел-без-спл-ч-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/5d8/5d8823670ac93f0a1a296675b9b7e077.jpg" rel="r-11-031"><img alt="пл-рел-без-спл-ч-рв.jpg" src="/upload/medialibrary/5d8/5d8823670ac93f0a1a296675b9b7e077.jpg" title="пл-рел-без-спл-ч-рв.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
	 Вес-<b>12,2 гр.</b> диаметр-<b>29,3 мм.</b>
</p>
<p style="text-align: center;">
</p>
<p>
 <b>&nbsp;</b> Вензель на полковничьем погоне Императора Николая II неразборчив. Крест на шее Михаила. Отсутствие сплошной черты. Пунктирная линия на аверсе начинается с точки и заканчивается точкой. <br>
</p>
<p>
 <br>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/751/7511e4688109600c0406da61da0c39e6.jpg" rel="18-06-2020-one"><img alt="ДР-лот-390-ав.jpg" src="/upload/medialibrary/751/7511e4688109600c0406da61da0c39e6.jpg" title="ДР-лот-390-ав.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/61b/61b09bb27003907b65600b3718a0125e.jpg" rel="18-06-2020-one"><img alt="ДР-лот-390-ухо.jpg" src="/upload/medialibrary/61b/61b09bb27003907b65600b3718a0125e.jpg" title="ДР-лот-390-ухо.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/d33/d3349d878dc45ac0e33cc33b84c6fad1.jpg" rel="18-06-2020-one"><img alt="ДР-лот-390-рв.jpg" src="/upload/medialibrary/d33/d3349d878dc45ac0e33cc33b84c6fad1.jpg" title="ДР-лот-390-рв.jpg" width="180" height="180" align="middle"></a>
</p>
<p>
</p>
<p style="text-align: center;">
	 Вес-<b>14,7 гр.</b> диаметр-<b>28,3 мм.</b>
</p>
<p>
</p>
<p>
</p><p style="text-align: center;">&nbsp; Вензель на погоне Императора Николая II нечетко выражен, крест на шапке Михаила не касается черты.Пунктирная линия расположена у самого борта медали , начинается со штриха им же и заканчивается . <br></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/ff6/ff6a1af38da0837b8796ebf051eadac4.jpg" rel="17.08.2021-1"><img alt="300-лет-ДР-аверс.jpg" src="/upload/medialibrary/ff6/ff6a1af38da0837b8796ebf051eadac4.jpg" title="300-лет-ДР-аверс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/375/3757c94c90ef93cb6a68ecae7d2a9c7d.jpg" rel="17.08.2021-1"><img alt="300-лет-ДР-ухо.jpg" src="/upload/medialibrary/375/3757c94c90ef93cb6a68ecae7d2a9c7d.jpg" title="300-лет-ДР-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/142/1421eedeb69033c082024a23c3a18c45.jpg" rel="17.08.2021-1"><img alt="300-лет-ДР-реверс.jpg" src="/upload/medialibrary/142/1421eedeb69033c082024a23c3a18c45.jpg" title="300-лет-ДР-реверс.jpg" width="185" height="185" align="middle"></a></p><p style="text-align: center;">Вес-<b>14,3 гр.</b> диаметр-<b>29,48 мм.</b><br></p><div>Вензель на полковничьем погоне Императора Николая II отсутствует, так же
 отсутствует сплошная черта.Портретное сходство Императоров 
приблизительное.<br>
Пунктирная линия на аверсе начинается с точки ею же и заканчивается. </div><p></p><b> </b>
<p>
</p>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>