<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Николай I");
?><div style="background-color: white">
<h2 style="text-align: center;"><b>Период царствования Николая I &nbsp; &nbsp; <br>
 </b></h2>
<p style="text-align: center;">
</p>
<h2 style="text-align: center;"><b>(1825-1855 )</b></h2>
<p style="text-align: center;">
 <a href="/upload/medialibrary/faa/faaaebf30237a32f05aece8dadfa0def.jpg" rel="g1-19-02-2020"><img alt="ЗООСА-115-ав-нов.jpg" src="/upload/medialibrary/faa/faaaebf30237a32f05aece8dadfa0def.jpg" title="ЗООСА-115-ав-нов.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/44f/44fbf6ff979539015d010428033bec21.jpg" rel="g1-19-02-2020"><img alt="ЗООСА-11.588--ухо-нов.jpg" src="/upload/medialibrary/44f/44fbf6ff979539015d010428033bec21.jpg" title="ЗООСА-11.588--ухо-нов.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/e25/e258a68baade2f7e9d645746442edc98.jpg" rel="g1-19-02-2020"><img alt="ЗООСА-115.588-реверс-нов.jpg" src="/upload/medialibrary/e25/e258a68baade2f7e9d645746442edc98.jpg" title="ЗООСА-115.588-реверс-нов.jpg" width="185" height="185" align="middle"></a>
</p>
<p>
 <b><b>Знак отличия ордена Святой Анны №115.588</b> </b>СПб монетный двор, серебро 72 пробы. <b><br>
 </b>
</p><div>
 Изображение предоставил<b> <b>Андрей Д.</b></b></div>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/b05/b0555fa0ed3c378493e2647828dc4c6b.jpg" rel="26.05.2021.1"><img alt="ЗООСА-133.993-ав.jpg" src="/upload/medialibrary/b05/b0555fa0ed3c378493e2647828dc4c6b.jpg" title="ЗООСА-133.993-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/087/087b59414045b3a67d9498673349ecd7.jpg" rel="26.05.2021.1"><img alt="ЗООСА-133.993-ухо.jpg" src="/upload/medialibrary/087/087b59414045b3a67d9498673349ecd7.jpg" title="ЗООСА-133.993-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/23e/23e14e380f72109beb25a75ed5aa6b59.jpg" rel="26.05.2021.1"><img alt="ЗООСА-133.993-рв.jpg" src="/upload/medialibrary/23e/23e14e380f72109beb25a75ed5aa6b59.jpg" title="ЗООСА-133.993-рв.jpg" width="185" height="185" align="middle"></a></p><p>
 <b><b>Знак отличия ордена Святой Анны №133.993</b> </b>СПб монетный двор, серебро 72 пробы, вес 8,59 гр., толщина 2,37 мм., размер 30,81х23,71мм., ухо 6,13 мм., корона 8,89 мм. <b><br></b></p><p><br>Изображение предоставил<b><b> <b>А.Б.</b></b></b></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/b4c/b4c4ea37d3e29e544ce08e952db7dcf2.jpg" rel="3-17-08-2020"><img alt="ЗООСА-№169.560-ав.jpg" src="/upload/medialibrary/b4c/b4c4ea37d3e29e544ce08e952db7dcf2.jpg" title="ЗООСА-№169.560-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/0d4/0d4b43be1c3f94cfa399f3f2f95175ec.jpg" rel="3-17-08-2020"><img alt="ЗООСА-№169.560-ухо.jpg" src="/upload/medialibrary/0d4/0d4b43be1c3f94cfa399f3f2f95175ec.jpg" title="ЗООСА-№169.560-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/989/989377d9c8953185f0f311ddf13ce26f.jpg" rel="3-17-08-2020"><img alt="ЗООСА-№169.560-рв.jpg" src="/upload/medialibrary/989/989377d9c8953185f0f311ddf13ce26f.jpg" title="ЗООСА-№169.560-рв.jpg" width="185" height="185" align="middle"></a></p><p style="text-align: center;"><br></p><p>
 <b><b>Знак отличия ордена Святой Анны №169.560</b> </b>СПб монетный двор, серебро 72 пробы, вес 8,16 гр., толщина 2,11 мм., размер 30,3х23,6 мм., ухо 6,0 мм., корона 9,7 мм. <b><br></b></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/036/036379326371369e77095a93cf541219.jpg" rel="26/05/2021/2"><img alt="ЗООСА-174.856-ав.jpg" src="/upload/medialibrary/036/036379326371369e77095a93cf541219.jpg" title="ЗООСА-174.856-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/745/7459fba7132f49f6812ec780a01d278d.jpg" rel="26/05/2021/2"><img alt="ЗООСА-174.856-ухо.jpg" src="/upload/medialibrary/745/7459fba7132f49f6812ec780a01d278d.jpg" title="ЗООСА-174.856-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/f4c/f4ca80f9d3df23ab9a1b8539b04e2c28.jpg" rel="26/05/2021/2"><img alt="ЗООСА-174.856-рв.jpg" src="/upload/medialibrary/f4c/f4ca80f9d3df23ab9a1b8539b04e2c28.jpg" title="ЗООСА-174.856-рв.jpg" width="185" height="185" align="middle"></a></p><p>
 <b><b>Знак отличия ордена Святой Анны №174.856</b> </b>СПб монетный двор, серебро 72 пробы, вес 8,33 гр., толщина 2,06 мм., размер 30,65х23,4 мм., ухо 5,96 мм., корона 9,15 мм. <b><br></b></p><p>Изображение предоставил<b><b> <b>А.Б.</b></b></b></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/446/4461396ce1e2533797512b9b6749b9e1.jpg" rel="4-17-08-2020"><img alt="ЗООСА-№172.578-ав.jpg" src="/upload/medialibrary/446/4461396ce1e2533797512b9b6749b9e1.jpg" title="ЗООСА-№172.578-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/68c/68c3e891ad063817ad793a4bbdbc2da9.jpg" rel="4-17-08-2020"><img alt="ЗООСА-№172.578-ухо.jpg" src="/upload/medialibrary/68c/68c3e891ad063817ad793a4bbdbc2da9.jpg" title="ЗООСА-№172.578-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/a93/a938bb90960e015c99066373918266c1.jpg" rel="4-17-08-2020"><img alt="ЗООСА-№172.578-рв.jpg" src="/upload/medialibrary/a93/a938bb90960e015c99066373918266c1.jpg" title="ЗООСА-№172.578-рв.jpg" width="185" height="185" align="middle"></a></p><p style="text-align: center;"><br></p><p>
 <b><b>Знак отличия ордена Святой Анны №182.578</b> </b>СПб монетный двор, серебро 72 пробы, вес 7,73 гр., толщина 2,13 мм., размер 30,7х23,8 мм., ухо 6,41 мм., корона 10,1 мм. <b><br>
 </b>
</p><p style="text-align: center;"><b>
 </b>
</p>
<p style="text-align: center;">
</p>
<hr>
 <a href="/upload/medialibrary/037/037084fae5007e27c948af4f49ad1c65.jpg" rel="gr24/10/19/01"></a>
<p style="text-align: center;">
 <a href="/upload/medialibrary/037/037084fae5007e27c948af4f49ad1c65.jpg" rel="gr24/10/19/01"><img alt="ЗООСА-184.627-аверс.jpg" src="/upload/medialibrary/037/037084fae5007e27c948af4f49ad1c65.jpg" title="ЗООСА-184.627-аверс.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/491/49156c8e8e5bfd2fdda22bd21fe7430d.jpg" rel="gr24/10/19/01"><img alt="ЗООСА-184.627-ухо-м.jpg" src="/upload/medialibrary/491/49156c8e8e5bfd2fdda22bd21fe7430d.jpg" title="ЗООСА-184.627-ухо-м.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/17c/17c372befd9f49eccd507e035c47d638.jpg" rel="gr24/10/19/01"><img alt="ЗООСА-184.627-реверс.jpg" src="/upload/medialibrary/17c/17c372befd9f49eccd507e035c47d638.jpg" title="ЗООСА-184.627-реверс.jpg" width="180" height="180" align="middle"></a>
</p>
 <a href="/upload/medialibrary/17c/17c372befd9f49eccd507e035c47d638.jpg" rel="gr24/10/19/01"></a>
<p>
</p>
<p style="text-align: center;">
 <b><b>Знак отличия ордена Святой Анны №184.627</b> </b>СПб монетный двор, серебро 72 пробы, вес 8,63 гр., толщина 2,28 мм., размер 30,7х23,7 мм., ухо 6,11 мм., корона 9,7 мм. <b><br>
 </b>
</p>
<div>
	 Изображение предоставил<b> <b>А.Б.</b></b>
	<hr>
</div>
 <p style="text-align: center;"><a href="/upload/medialibrary/ce5/ce54621dfd3dcd1339d77f49c630549c.jpg" rel="25-05-2021-2"><img alt="ЗООСА-198624-ав.jpg" src="/upload/medialibrary/ce5/ce54621dfd3dcd1339d77f49c630549c.jpg" title="ЗООСА-198624-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/bc5/bc5ca73b253a1a66e42f51557d2890c5.jpg" rel="25-05-2021-2"><img alt="ЗООСА-198624-ухо.jpg" src="/upload/medialibrary/bc5/bc5ca73b253a1a66e42f51557d2890c5.jpg" title="ЗООСА-198624-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/45c/45cd7090ea36cf2604633d3fbec4043d.jpg" rel="25-05-2021-2"><img alt="ЗООСА-198624-рв.jpg" src="/upload/medialibrary/45c/45cd7090ea36cf2604633d3fbec4043d.jpg" title="ЗООСА-198624-рв.jpg" width="185" height="185" align="middle"></a></p><p>
 </p><p></p><p></p><p style="text-align: center;"><b><b>Знак отличия ордена Святой Анны №198.624</b> </b>СПб монетный двор, серебро 72 пробы, вес 9,46 гр., толщина 2,28 мм., размер 30,5х23,24 мм., ухо 5,58 мм., корона 9,99 мм. </p><p>Изображение предоставил<b> <b>А.Б.</b></b></p>
<hr>
<p style="text-align: center;"><b><a href="/upload/medialibrary/648/6480fde1d6157346c8a1cf38f48a7efd.jpg" rel="23.08.2021-1"><img alt="ЗООСА-218042-ав.jpg" src="/upload/medialibrary/648/6480fde1d6157346c8a1cf38f48a7efd.jpg" title="ЗООСА-218042-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/cea/ceaeff5e9e2fbd5cafdf9ff997529bc1.jpg" rel="23.08.2021-1"><img alt="ЗООСА-218042-ухо-2.jpg" src="/upload/medialibrary/cea/ceaeff5e9e2fbd5cafdf9ff997529bc1.jpg" title="ЗООСА-218042-ухо-2.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/09d/09d2e7d180e4fb7dabea9aa88693294e.jpg" rel="23.08.2021-1"><img alt="ЗООСА-218042-рв.jpg" src="/upload/medialibrary/09d/09d2e7d180e4fb7dabea9aa88693294e.jpg" title="ЗООСА-218042-рв.jpg" width="185" height="185" align="middle"></a><br></b></p><p style="text-align: center;"><br></p><p><b><b>Знак отличия ордена Святой Анны №218.042</b> </b>СПб монетный двор, серебро 72 пробы, вес 8,81 гр., толщина 2,16 мм., размер 30,26х23,14 мм., ухо 5,94 мм., корона 9,99 мм. </p><p>Изображение предоставил<b> <b>А.Б.</b></b>
 
</p><p style="text-align: center;">
 
</p>
<hr>
<p></p><p></p><p></p>
<p style="text-align: center;">
 <a href="/upload/medialibrary/387/387e95b482911b48e144abd46a1c5388.jpg" rel="gr24/10-1"><img alt="ЗООСА-304115-аверс.jpg" src="/upload/medialibrary/387/387e95b482911b48e144abd46a1c5388.jpg" title="ЗООСА-304115-аверс.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/413/41354ee823f09a044d4517e138c29bef.jpg" rel="gr24/10-1"><img alt="ЗООСА-304115-ухо.jpg" src="/upload/medialibrary/413/41354ee823f09a044d4517e138c29bef.jpg" title="ЗООСА-304115-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/98f/98f138de8d8fc2c70b966f1acd2d284a.jpg" rel="gr24/10-1"><img alt="ЗООСА-304115-реверс.jpg" src="/upload/medialibrary/98f/98f138de8d8fc2c70b966f1acd2d284a.jpg" title="ЗООСА-304115-реверс.jpg" width="180" height="180" align="middle"></a>
</p>
 <a href="/upload/medialibrary/98f/98f138de8d8fc2c70b966f1acd2d284a.jpg"></a>
<p>
 <br>
</p>
<p>
 <b><b>Знак отличия ордена Святой Анны №304.115</b> </b>СПб монетный двор, серебро 72 пробы, вес 9,51 гр., толщина 2,21 мм., размер 31х23,6 мм., ухо 5,31 мм., корона 9,2 мм. <b><br></b></p>
<hr>
<p style="text-align: center;"><b><a href="/upload/medialibrary/6e2/6e2062dd41afbb7e45cc6e5334cadc34.jpg" rel="gr12/04/21"><img alt="ЗООСА-352.024-ав.jpg" src="/upload/medialibrary/6e2/6e2062dd41afbb7e45cc6e5334cadc34.jpg" title="ЗООСА-352.024-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/2fb/2fb130813e7ba48d6d361dd0bf54f0f2.jpg" rel="gr12/04/21"><img alt="ЗООСА-352.024-ухо.jpg" src="/upload/medialibrary/2fb/2fb130813e7ba48d6d361dd0bf54f0f2.jpg" title="ЗООСА-352.024-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/ad3/ad3204fab1cdd341c6760c62177cf638.jpg" rel="gr12/04/21"><img alt="ЗООСА-352.024-рв.jpg" src="/upload/medialibrary/ad3/ad3204fab1cdd341c6760c62177cf638.jpg" title="ЗООСА-352.024-рв.jpg" width="185" height="185" align="middle"></a><br></b></p><p style="text-align: center;"><b>

</b></p>
<p><b>Знак отличия ордена Святой Анны №352.024 </b>СПб монетный двор, серебро
72 пробы, вес 9,91 гр., толщина 2,54 мм., размер 31,09х23,68 мм., ухо 5,28 мм.,
корона 8,98 мм. </p>

<p> </p>

<p>Знаком
отличия за №352.024 награжден<b> Борисов Семион</b> – рядовой, Ревельского егерского
полка. «За 20-летнюю беспорочную выслугу в войсках». Награжден знаком отличия
Св. Анны Высочайше утверждённому в 1841 г. по докладу Капитула Орденов.РГИА,
496 фонд, 3 опись.</p><p>Изображение предоставил <b>А.Б.</b></p>
<hr>
<p style="text-align: center;"><b><a href="/upload/medialibrary/45c/45ce189686e26db82a0e8c2bdd34fab6.jpg" rel="18.08.2021-3"><img alt="ЗООСА-360868-ав.jpg" src="/upload/medialibrary/45c/45ce189686e26db82a0e8c2bdd34fab6.jpg" title="ЗООСА-360868-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/876/8760ecf5be513db473c896e975827030.jpg" rel="18.08.2021-3"><img alt="ЗООСА-360868-ухо.jpg" src="/upload/medialibrary/876/8760ecf5be513db473c896e975827030.jpg" title="ЗООСА-360868-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/6ef/6ef3504c2e5a7eeadbc226f5a9f9f6ec.jpg" rel="18.08.2021-3"><img alt="ЗООСА-360868-рв.jpg" src="/upload/medialibrary/6ef/6ef3504c2e5a7eeadbc226f5a9f9f6ec.jpg" title="ЗООСА-360868-рв.jpg" width="185" height="185" align="middle"></a><br></b></p><div><b>Знак отличия ордена Святой Анны №360.868</b> СПб монетный двор, серебро
72 пробы, вес 9,01 гр., толщина 2,16 мм., размер 30,8х23,88 мм., ухо 5,57 мм.,
корона 8,65 мм. <br></div>Изображение предоставил <b>А.Б.</b><p style="text-align: center;"><b>
 </b>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/3c8/3c89bcfd0c2344c33ff615aa57b80e49.jpg" rel="gr24/10/02"><img alt="ЗООСА-376.872-аверс.jpg" src="/upload/medialibrary/3c8/3c89bcfd0c2344c33ff615aa57b80e49.jpg" title="ЗООСА-376.872-аверс.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/71d/71d90f5d2946c52220e4f94389ee302e.jpg" rel="gr24/10/02"><img alt="ЗООСА-376.872-ухо.jpg" src="/upload/medialibrary/71d/71d90f5d2946c52220e4f94389ee302e.jpg" title="ЗООСА-376.872-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/6d8/6d8480f0ccbbbe2a8980371369b899f5.jpg" rel="gr24/10/02"><img alt="ЗООСА-376.872-реверс.jpg" src="/upload/medialibrary/6d8/6d8480f0ccbbbe2a8980371369b899f5.jpg" title="ЗООСА-376.872-реверс.jpg" width="180" height="180" align="middle"></a>
</p>
<p style="text-align: center;">
 <br>
</p>
<p>
 <b><b>Знак отличия ордена Святой Анны №376.872</b> </b>СПб монетный двор, серебро 72 пробы, вес 9,11 гр., толщина 2,25 мм., размер 31,5х23,8 мм., ухо 5,81 мм., корона 9,1 мм. <b><br>
 </b>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/d9a/d9a1e1339d28937b60e2690da77cff03.jpg" rel="gr24/10/04"><img alt="ЗООСА-385.706-аверс.jpg" src="/upload/medialibrary/d9a/d9a1e1339d28937b60e2690da77cff03.jpg" title="ЗООСА-385.706-аверс.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/952/95256f7b8a9aebcf561bf6b7b1adec57.jpg" rel="gr24/10/04"><img alt="ЗООСА-385.706-ухо.jpg" src="/upload/medialibrary/952/95256f7b8a9aebcf561bf6b7b1adec57.jpg" title="ЗООСА-385.706-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/77b/77becfd76d9dbdd2d96920f9f9fb847e.jpg" rel="gr24/10/04"><img alt="ЗООСА-385.706-реверс.jpg" src="/upload/medialibrary/77b/77becfd76d9dbdd2d96920f9f9fb847e.jpg" title="ЗООСА-385.706-реверс.jpg" width="180" height="180" align="middle"></a>
</p>
<p style="text-align: center;">
 <br>
</p>
<p>
 <b><b>Знак отличия ордена Святой Анны №385.706</b> </b>СПб монетный двор, серебро 72 пробы, вес 9,11 гр., толщина 2,25 мм., размер 31х24,1 мм., ухо 5,12 мм., корона 9,14 мм. <b><br>
 </b>
</p>
<div>
	 &nbsp;&nbsp;&nbsp; Знаком отличия за №385.706 награжден<b> <b>Федор Гридин</b><span style="font-weight: 400;">
	– унтер-офицер, Лейб-гвардии московского полка.«За 20-летнюю беспорочную выслугу в войсках».Награжден знаком отличия Св. Анны Высочайше утверждённому 28 октября 1850 г. по докладу Капитула Орденов.РГИА, 496 фонд, 3 опись, дело 359, л.л.64,136/об.</span> </b>
</div>
 Изображение предоставил <b>Геннадий Мазяркин.</b><br>
<hr>
<p style="text-align: center;">
 <b><a href="/upload/medialibrary/3ab/3ab9ece2483510d44fc07109b23e413c.jpg" rel="gr25.10.01"><img alt="ЗООСА-390647-аверс.jpg" src="/upload/medialibrary/3ab/3ab9ece2483510d44fc07109b23e413c.jpg" title="ЗООСА-390647-аверс.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/f29/f297b466900f6465e896851fb60a0509.jpg" rel="gr25.10.01"><img alt="ЗООСА-390647-ухо.jpg" src="/upload/medialibrary/f29/f297b466900f6465e896851fb60a0509.jpg" title="ЗООСА-390647-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/c9f/c9fde70ef8c37228099b5be8c354d5de.jpg" rel="gr25.10.01"><img alt="ЗООСА-390647-реверс.jpg" src="/upload/medialibrary/c9f/c9fde70ef8c37228099b5be8c354d5de.jpg" title="ЗООСА-390647-реверс.jpg" width="180" height="180" align="middle"></a><br>
 </b>
</p>
<p style="text-align: center;">
 <br>
</p>
<p>
 <b><b>Знак отличия ордена Святой Анны №390.647</b> </b>СПб монетный двор, серебро 72 пробы, вес 9,26 гр., толщина 2,12 мм., размер 31,3х23,9 мм., ухо 5,5 мм., корона 9,2 мм. <b><br>
 </b>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/d74/d748b2a9a93a765c2af3b77db3d8b3de.jpg" rel="G1-13-02-2020"><img alt="ЗООСА-424-б.jpg" src="/upload/medialibrary/d74/d748b2a9a93a765c2af3b77db3d8b3de.jpg" title="ЗООСА-424-б.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/99a/99af1993814ae9840fe1e8ac03d17df2.jpg" rel="G1-13-02-2020"><img alt="ЗООСА-424.278-ухо-2б.jpg" src="/upload/medialibrary/99a/99af1993814ae9840fe1e8ac03d17df2.jpg" title="ЗООСА-424.278-ухо-2б.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/480/480f014a4339994ca56828507aed1faf.jpg" rel="G1-13-02-2020"><img alt="ЗООСА-424.278-реверс-б.jpg" src="/upload/medialibrary/480/480f014a4339994ca56828507aed1faf.jpg" title="ЗООСА-424.278-реверс-б.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
 <br>
</p>
<p>
 <b><b>Знак отличия ордена Святой Анны №424.278</b> </b>СПб монетный двор, серебро 72 пробы.<b><br>
 </b>
</p>

<p style="text-align: center;">
 <b> </b>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/d5a/d5a00dc6544a03c8a67b55e665ef61ff.jpg" rel="2-02-2020"><img alt="ЗООСА-424.513-аверс.jpg" src="/upload/medialibrary/d5a/d5a00dc6544a03c8a67b55e665ef61ff.jpg" title="ЗООСА-424.513-аверс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/de5/de5d201a12df82a8367116432596d664.jpg" rel="2-02-2020"><img alt="ЗООСА-424.513-ухо.jpg" src="/upload/medialibrary/de5/de5d201a12df82a8367116432596d664.jpg" title="ЗООСА-424.513-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/50e/50efb8402be9506a7c763320d534fff9.jpg" rel="2-02-2020"><img alt="ЗООСА-424.513-реверс.jpg" src="/upload/medialibrary/50e/50efb8402be9506a7c763320d534fff9.jpg" title="ЗООСА-424.513-реверс.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
 <br>
</p>
<p>
 <b><b>Знак отличия ордена Святой Анны №424.513</b> </b>СПб монетный двор, серебро 84 пробы<b>.<br></b></p><p><b><br></b></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/e87/e8782c5908c3106f02df0f446c7fa2b8.jpg" rel="29-1-2020"><img alt="ЗООСА-428.677-ав.jpg" src="/upload/medialibrary/e87/e8782c5908c3106f02df0f446c7fa2b8.jpg" title="ЗООСА-428.677-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/b81/b8146896ce3650836c74788b87147ab1.jpg" rel="29-1-2020"><img alt="ЗООСА-428.677-ухо.jpg" src="/upload/medialibrary/b81/b8146896ce3650836c74788b87147ab1.jpg" title="ЗООСА-428.677-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/1f0/1f0e44bfaa8a1c6cd79b03f32d4fdcf4.jpg" rel="29-1-2020"><img alt="ЗООСА-428.677-рв.jpg" src="/upload/medialibrary/1f0/1f0e44bfaa8a1c6cd79b03f32d4fdcf4.jpg" title="ЗООСА-428.677-рв.jpg" width="185" height="185" align="middle"></a></p><p style="text-align: center;"></p><p style="text-align: center;"><b>Знак отличия ордена Святой Анны №428.677 </b>СПб монетный двор, серебро 84 
пробы, вес 9,11 гр., толщина 2,21 мм., размер 30,8х23,8 мм., ухо 5,61 
мм., корона 9 мм.</p><p></p><p style="text-align: center;"></p><p style="text-align: left;">Изображение предоставил <b>А.Б.</b></p><p style="text-align: center;"></p><p></p>
<hr>
<p><b><br></b></p><p style="text-align: center;"><b><a href="/upload/medialibrary/193/193667b68cfe4e68d7e416ea2ec552b1.jpg" rel="1-1-2-1"><img alt="ЗООСА-435.302-авс.jpg" src="/upload/medialibrary/193/193667b68cfe4e68d7e416ea2ec552b1.jpg" title="ЗООСА-435.302-авс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/f41/f416d8763b283f710185583aa73142cd.jpg" rel="1-1-2-1"><img alt="ЗООСА-435.302-ухо.jpg" src="/upload/medialibrary/f41/f416d8763b283f710185583aa73142cd.jpg" title="ЗООСА-435.302-ухо.jpg" width="165" height="165" align="middle"></a><a href="/upload/medialibrary/13e/13e947b61cbcae7fea97b6ec10e62815.jpg" rel="1-1-2-1"><img alt="ЗООСА-435.302-рвс.jpg" src="/upload/medialibrary/13e/13e947b61cbcae7fea97b6ec10e62815.jpg" title="ЗООСА-435.302-рвс.jpg" width="185" height="185" align="middle"></a><br></b></p><p style="text-align: center;"><br></p><p>
 <b><b>Знак отличия ордена Святой Анны №435.302</b> </b>СПб монетный двор, серебро 72 пробы, вес 8,65 гр., толщина 2,21 мм., размер 31,3х24,2 мм., ухо 5,7 мм., корона 9,8 мм. <b><br></b></p><p>Изображение предоставил<b> <b>А.Б.</b></b></p><p><b><b><br></b>
 </b>
</p><p style="text-align: center;"><b>
 </b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/656/656c84c35d2d48d2fd66d08c79b83c11.jpg" rel="GR25/10/x"><img alt="ЗОСА--439775-аверс.jpg" src="/upload/medialibrary/656/656c84c35d2d48d2fd66d08c79b83c11.jpg" title="ЗОСА--439775-аверс.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/2a9/2a9a84e7ac2e0d8d2094b09b44a0b8cf.jpg" rel="GR25/10/x"><img alt="ЗОСА--439775-ухо.jpg" src="/upload/medialibrary/2a9/2a9a84e7ac2e0d8d2094b09b44a0b8cf.jpg" title="ЗОСА--439775-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/2e4/2e4d6654ec7bf24f22ff8ff5c8701ac3.jpg" rel="GR25/10/x"><img alt="ЗОСА--439775-реверс.jpg" src="/upload/medialibrary/2e4/2e4d6654ec7bf24f22ff8ff5c8701ac3.jpg" title="ЗОСА--439775-реверс.jpg" width="180" height="180" align="middle"></a>
</p>
<p style="text-align: center;">
 <br>
</p>
<p>
 <b><b>Знак отличия ордена Святой Анны №439.775</b> </b>СПб монетный двор, серебро 72 пробы, вес 9,91 гр., толщина 2,65 мм., размер 31,1х23,9 мм., ухо 5,8 мм., корона 9,5 мм. <b><br>
 </b>
</p>
<div>
	 &nbsp;&nbsp;&nbsp; Знаком отличия за №439.775 награжден <b>Леонтий Карманов</b><span style="font-weight: 400;"> – рядовой, Лейб-гвардии Резервного Саперного батальона.«За 20-летнюю беспорочную строевую службу».Награжден знаком отличия Св. Анны Высочайше утверждённому 3 ноября 1856 г. по докладу Капитула Орденов. Знаки сии направлены в Инспекторский Департамент Военного Министерства.РГИА, 496 фонд, 3 опись, дело 372, 281 страница.</span>
</div>
<div>
 <span style="font-weight: 400;">Изображение предоставил<b> <b>А.Б.</b></b></span>
</div>
<p style="text-align: center;">
 <b> </b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p style="text-align: center;">
 <b> </b>
</p>
<p>
</p>
<hr>
<h2></h2>
<p style="text-align: center;">
</p>
<h2></h2>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>