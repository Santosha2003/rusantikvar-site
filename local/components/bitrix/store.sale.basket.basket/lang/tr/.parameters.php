<?
$MESS ['SBB_DESC_YES'] = "Evet";
$MESS ['SBB_DESC_NO'] = "Hayэr";
$MESS ['SBB_PATH_TO_ORDER'] = "Sipariю dьzenleme sayfasэ";
$MESS ['SBB_HIDE_COUPON'] = "Kupon giriю alanэnэ gizle";
$MESS ['SBB_COLUMNS_LIST'] = "Gцrьntьlenen sьtьnler";
$MESS ['SBB_BNAME'] = "Ьrьn adэ";
$MESS ['SBB_BPRICE'] = "Fiyat";
$MESS ['SBB_BTYPE'] = "Tanэmэ";
$MESS ['SBB_BQUANTITY'] = "Sayэ";
$MESS ['SBB_BDELETE'] = "Sil";
$MESS ['SBB_BDELAY'] = "Beklet";
$MESS ['SBB_BWEIGHT'] = "Aрэrlэk";
$MESS ['SBB_BDISCOUNT'] = "Эndirim";
$MESS ['SBB_WEIGHT_UNIT'] = "Aрэrlэk birimi";
$MESS ['SBB_WEIGHT_UNIT_G'] = "g";
$MESS ['SBB_WEIGHT_KOEF'] = "Gram iзin birim katsayэsэ";
$MESS ['SBB_QUANTITY_FLOAT'] = "Sayэnэn kэsmi deрerini kullanэn";
$MESS ['SBB_VAT_SHOW_VALUE'] = "KDV deрerini gцrьntьle";
$MESS ['SBB_VAT_INCLUDE'] = "KDV'yэ fiyata dahil et";
$MESS ['SBB_COUNT_DISCOUNT_4_ALL_QUANTITY'] = "Her црe iзin indirimi hesaplayэn (tьm ьrьn miktarэ iзin)";
$MESS ['SOF_COUNT_DISCOUNT_4_ALL_QUANTITY'] = "Her црe iзin indirimi hesaplayэn (tьm ьrьn miktarэ iзin)";
?>