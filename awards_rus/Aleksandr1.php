<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Александр I");
?><div style="background-color: white">
<h2 style="text-align: center;"><b>Период царствования Александра I &nbsp; &nbsp; <br>
 </b></h2>
<p style="text-align: center;">
</p>
<h2 style="text-align: center;"><b>(1801-1825 )</b></h2>
 <a href="/upload/medialibrary/a4e/a4eae8496556bcabc6339ad7b22c77d9.jpg" rel="group111"></a>
<p style="text-align: center;">
 <a href="/upload/medialibrary/a4e/a4eae8496556bcabc6339ad7b22c77d9.jpg" rel="group111"><img alt="peter1" src="/upload/medialibrary/a4e/a4eae8496556bcabc6339ad7b22c77d9.jpg" title="75.322 ав.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/ac2/ac2d197c00c34873e21975a0b46e34ac.jpg" rel="group111"><img alt="peter1" src="/upload/medialibrary/ac2/ac2d197c00c34873e21975a0b46e34ac.jpg" title="ЗООСА-75322-ухо-св.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/a33/a334fe6441b66f44a4c32f948e284ac7.jpg" rel="group111"><img alt="peter1" src="/upload/medialibrary/a33/a334fe6441b66f44a4c32f948e284ac7.jpg" title="75.322 рев.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: left;">
 <b>&nbsp; Знак отличия ордена Святой Анны №75.322</b> СПб монетный двор, серебро 72 пробы, вес 7,3 гр., толщина 1,92 мм., размер 30,3х22,9 мм., ухо 6,1 мм., корона 10,1 мм. <br>
</p>
<p style="text-align: left;">
	 &nbsp;&nbsp; Знаком отличия за №75.322 награжден<b> Максимов Павел</b>
	– рядовой, 4-го Морского полка. "За 20-летнюю беспорочную строевую службу".Высочайше утвержденному 29 декабря 1816 г. по докладу Капитула Орденов.РГИА, 496 фонд, 3 опись, 290 дело, Лист 47 <br>
</p>
<p style="text-align: left;">
	 Изображение предоставил <b>А.Б.</b></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/a2f/a2f13155402753a9a3288688481f09ed.jpg" rel="2-17-08-2020"><img alt="ЗООСА-№79.657-ав.jpg" src="/upload/medialibrary/a2f/a2f13155402753a9a3288688481f09ed.jpg" title="ЗООСА-№79.657-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/590/5908156018fa918f8784c9713b339095.jpg" rel="2-17-08-2020"><img alt="ЗООСА-№79.657-ухо.jpg" src="/upload/medialibrary/590/5908156018fa918f8784c9713b339095.jpg" title="ЗООСА-№79.657-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/fa8/fa840578cff46a802a459ea2c2397ddc.jpg" rel="2-17-08-2020"><img alt="ЗООСА-№79.657-рв.jpg" src="/upload/medialibrary/fa8/fa840578cff46a802a459ea2c2397ddc.jpg" title="ЗООСА-№79.657-рв.jpg" width="185" height="185" align="middle"></a></p><p style="text-align: center;"><br><b><b>Знак отличия ордена Святой Анны №79.657</b> </b>СПб монетный двор, серебро 72 пробы, вес 7,42 гр., толщина 2,11 мм., размер 30,1х23,1 мм., ухо 6,2 мм., корона 9,6 мм. <br></p><p style="text-align: center;">&nbsp;Знаком отличия за №79.657 награжден<b> Миронов Сафрон</b>
	– бомбардир, 18-й Артбригады. "За 20-летнюю беспорочную строевую 
службу".Высочайше утвержденному 29 декабря 1816 г. по докладу Капитула 
Орденов.РГИА, 496 фонд, 3 опись, 290 дело, Лист 292</p>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/0bc/0bc053ca624c42c52f00f7b3fbd20410.jpg" rel="group21"><img alt="ЗОСА-81763-аверс-н.jpg" src="/upload/medialibrary/0bc/0bc053ca624c42c52f00f7b3fbd20410.jpg" title="ЗОСА-81763-аверс-н.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/815/815b3bb3f75155caba5ff4a75c8d5d95.jpg" rel="group21"><img alt="ЗОСА-81763-ухо.jpg" src="/upload/medialibrary/815/815b3bb3f75155caba5ff4a75c8d5d95.jpg" title="ЗОСА-81763-ухо.jpg" width="165" height="165" align="middle"></a><a href="/upload/medialibrary/4a8/4a8b4d97c94377ecc1885f30030c1377.jpg" rel="group21"><img alt="ЗОСА-81763-реверс-н.jpg" src="/upload/medialibrary/4a8/4a8b4d97c94377ecc1885f30030c1377.jpg" title="ЗОСА-81763-реверс-н.jpg" width="180" height="180" align="middle"></a>
</p>
<p style="text-align: left;">
 <b>Знак отличия ордена Святой Анны №81.762</b> СПб монетный двор, серебро 72 пробы, вес 8,1 гр., толщина 2,16 мм., размер 30,1х22,9 мм., ухо 5,31 мм., корона 10,1 мм. <br>
</p>
<p style="text-align: left;">
	 Знаком отличия за №81.762 награжден<b> Василий Федотов</b>
	– рядовой,Суздальского пехотного&nbsp; полка. "За 20-летнюю беспорочную и безотлучную службу в войсках".Высочайше утвержденному в 1817 г. (месяц и число не установлены) по докладу Капитула Орденов.РГИА, 496 фонд, 3 опись <br>
</p>

<p style="text-align: left;">
	 Изображение предоставил <b>А.Б.</b></p>
<hr>
<p style="text-align: center;"><a href="/upload/medialibrary/879/8792d4b91d9d025c03434db84b4930c7.jpg" rel="17-08-2020-1"><img alt="ЗООСА-№84.159-ав.jpg" src="/upload/medialibrary/879/8792d4b91d9d025c03434db84b4930c7.jpg" title="ЗООСА-№84.159-ав.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/c43/c434bdef6ce61e4b9c84edcb6f491111.jpg" rel="17-08-2020-1"><img alt="ЗООСА-№84.159-ухо.jpg" src="/upload/medialibrary/c43/c434bdef6ce61e4b9c84edcb6f491111.jpg" title="ЗООСА-№84.159-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/514/514f59de4b0430bae46acd72a6ce4aaf.jpg" rel="17-08-2020-1"><img alt="ЗООСА-№84.159-рв.jpg" src="/upload/medialibrary/514/514f59de4b0430bae46acd72a6ce4aaf.jpg" title="ЗООСА-№84.159-рв.jpg" width="185" height="185" align="middle"></a></p><p style="text-align: center;"><br><b><b> Знак отличия ордена Святой Анны №84.159</b> </b>СПб монетный двор, серебро 72 пробы, вес 7,72 гр., толщина 2,21 мм., размер 30х23,1 мм., ухо 5,8 мм., корона 9,6 мм. 
</p>
<hr>
<p style="text-align: center;">
 <b><a href="/upload/medialibrary/3f5/3f51266c3550eebcf6558bb14f218edc.jpg" rel="group123456789"><img alt="peter111" src="/upload/medialibrary/3f5/3f51266c3550eebcf6558bb14f218edc.jpg" title="ЗООСА-95789-аверс-св.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/845/8452d58b53d2f8e4068471fc070fab82.jpg" rel="group123456789"><img alt="peter111" src="/upload/medialibrary/845/8452d58b53d2f8e4068471fc070fab82.jpg" title="ЗООСА-95789-ухо-св.jpg" width="180" height="180" align="middle"></a><a href="/upload/medialibrary/c51/c51543a5af9b94c0e97a9bd13becafb3.jpg" rel="group123456789"><img alt="peter111" src="/upload/medialibrary/c51/c51543a5af9b94c0e97a9bd13becafb3.jpg" title="ЗООСА-95789-реверс-св.jpg" width="180" height="180" align="middle"></a><br>
 </b>
</p>
<p style="text-align: center;">
 <br>
</p>
<p style="text-align: left;">
 <b><b>Знак отличия ордена Святой Анны №95.789</b> </b>СПб монетный двор, серебро 72 пробы, вес 7,9 гр., толщина 1,96 мм., размер 30,2х23,1 мм., ухо 6,11 мм., корона 10,1 мм. Знаком отличия за №95.789 награжден<b> Денис Матвеев</b> – рядовой, Ряжского пехотного полка.(Ныне состоит в 20-ом Егерьском полку).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</p>
<p style="text-align: left;">
	 С № 94207 по № 100184 награждены по докладу, Высочайше утвержденному в 17-й день февраля 1820 года. С № 94983 по № 97921 при отношении от 9 марта за № 862 в Инспекторский Департамент Главного Штаба Его Императорского Величества.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br>
</p>
<p style="text-align: left;">
	 РГИА, 496 фонд, 3 опись, 293 дело, 191 страница.
</p>
<p style="text-align: left;">
</p>
 Изображение предоставил<b> <b>А.Б.</b></b>
<hr>
<p style="text-align: center;">
 <a href="/upload/medialibrary/060/06025d4d3f053be7a29ce0fa11d4fc08.jpg" rel="2-20-02-2020"><img alt="ЗООСА-96.090-аверс.jpg" src="/upload/medialibrary/060/06025d4d3f053be7a29ce0fa11d4fc08.jpg" title="ЗООСА-96.090-аверс.jpg" width="185" height="185" align="middle"></a><a href="/upload/medialibrary/302/302155422c19d9115cb6335c64ac5001.jpg" rel="2-20-02-2020"><img alt="ЗООСА-96.090-ухо.jpg" src="/upload/medialibrary/302/302155422c19d9115cb6335c64ac5001.jpg" title="ЗООСА-96.090-ухо.jpg" width="175" height="175" align="middle"></a><a href="/upload/medialibrary/6c8/6c84b63072ee73d205dd80c67a7f56ce.jpg" rel="2-20-02-2020"><img alt="ЗООСА-96.090-реверс.jpg" src="/upload/medialibrary/6c8/6c84b63072ee73d205dd80c67a7f56ce.jpg" title="ЗООСА-96.090-реверс.jpg" width="185" height="185" align="middle"></a>
</p>
<p style="text-align: center;">
 <br>
 <b><b>Знак отличия ордена Святой Анны №96.090 </b></b>СПб монетный двор, серебро 72 пробы<b>.<br>
 </b>
</p>
<hr>
<h2></h2>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>